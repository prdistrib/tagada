#!/usr/bin/env python3

"""
    yaml graps to dot
"""

import os
import argparse

import yaml
from random import randint, random


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('file', type=argparse.FileType('r'))
	args = parser.parse_args()
	infile = args.file
	#print(infile)
	dirname = os.path.dirname(infile.name)
	basename = os.path.basename(infile.name)
	basename = os.path.splitext(basename)[0]
	data = yaml.load(infile, Loader=yaml.SafeLoader)
	nodes = data["nodes"]
	#print(nodes)
	print("digraph", '"' + basename + '"' , "{")
	for src in nodes:
		trans = nodes[src]["accessTo"]
		lbl = str(src)
		if "procDemand" in nodes[src]:
			lbl += '\\n' + str(nodes[src]["procDemand"])
		if "memSize" in nodes[src]:
			lbl += '\\n' + str(nodes[src]["memSize"])
		print(src + ' [label="' + lbl + '"];') 
		#print(trans)
		for t in trans:
			dest=t[0]
			datasz=t[1]
			print(src, '->', dest, '[label="' + str(datasz) + '"];') 
			# print(src, "-", datasz, "->", dest)
	print("}")
