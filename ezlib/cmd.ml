(*
Command-line + args helper
Usage:
1) customize if needed:
   - by changing default name and version
   - by adding positionnal and optionnal (i.e. "-xxx") arguments
2) run
*)

module P=Printf
module U=Utils
module V=Verbose

(* NAME AND VERSION *)
let toolversion: string ref = ref "0.0"
let toolpath: string ref = ref Sys.argv.(0)
let toolname: string ref = ref (Filename.basename !toolpath)
let toolgoal: string ref = ref ""
let tooldescription: string ref = ref ""

let set_name (n: string) = toolname := n
let set_version (v: string) = toolversion := v
let set_goal (s:string) = toolgoal := s
let set_description (s:string) = tooldescription := s

let tool_name () = !toolname
let tool_version () = !toolname

(* UNFLAGGED ARGS *)
let scatter_handler: (string -> unit) option ref = ref None
let scatter_finalize: (unit -> unit) ref = ref (fun () -> ())
let scatter_desc = ref ""

(* EXCEPTION TO CATCH IN run *)
exception Cmd_failure of string
let fmt = P.sprintf
let failure msg = raise (Cmd_failure msg)

(* TODO compat with old handler system: to be simplified *)
let check_handler () =
   if !scatter_handler <> None then 
      failure "Cmd.dflt_handler bad usage, define at most one handler\n"

(* multiple mandatory bypos args 
TODO compat with old handler system:
- scatter_handler must remain None
- at run, if scatter_handler is still None
  and by_pos is not empty,
  set the default handler as bypos-handler 
*)
type bypos = {
(* TODO what exactly must be stored to program the bypos_handler ? *)
   nme: string;
   dostore: string -> unit
}
let bypos_list: bypos list ref = ref []
let check_bypos () =
   if !bypos_list <> [] then 
      failure "Cmd.dflt_handler bad usage, define at most one handler\n"

let dostore_int (min:int) (max:int) (nme: string) (store: int ref): string -> unit =
fun raw -> try (
   let v = int_of_string raw in
   if v < min then
      failure (fmt "Error: argument '%s' must be greater than %d\n" nme min);
   if v > max then
      failure (fmt "Error: argument '%s' must be less than %d\n" nme max);
   store := v
) with _ -> (
   failure (fmt "Error: argument '%s', integer expected, get '%s'\n" nme raw);
)

let dostore_string (_nme: string) (store: string ref): string -> unit =
fun raw -> store := raw

let bypos_int ?(min=min_int) ?(max=max_int) (nme: string): (unit -> int) = 
   (* TODO compat with old handler system: must remain None until run *)
   check_handler ();
   (* allocate a int *)
   let store = ref 0 in
   let dostore = dostore_int min max nme store in 
   let handler = { nme; dostore } in
   bypos_list := handler::!bypos_list;
   (fun () -> !store)

let bypos_string (nme: string): (unit -> string) = 
   (* TODO compat with old handler system: must remain None until run *)
   check_handler ();
   (* allocate a string *)
   let store = ref "" in
   let dostore = dostore_string nme store in
   let handler = { nme; dostore } in
   bypos_list := handler::!bypos_list;
   (fun () -> !store)

(* finalize bypos and returns the suitable 'do_others' for Arg.parse *)
let unexpected s = failure (fmt "Error: unexpected argument '%s'\n" s)
let make_bypos_handler () : string -> unit =
   bypos_list := List.rev !bypos_list;
   scatter_desc := String.concat " "
      (List.map (fun x -> x.nme) !bypos_list); 
   let f () =
      match !bypos_list with
      | [] -> ()
      | ll ->
         let missings = List.map (fun x -> x.nme) ll in
         let missings = U.string_of_string_list ~obr:"" ~cbr:"" ~sep:"," missings in
         failure (fmt "Error: argument(s) %s missing\n" missings)
   in
   scatter_finalize := f;
   let h s = (
      match !bypos_list with
      | h::rest ->
         h.dostore s;
         bypos_list := rest;
      | [] -> unexpected s
   ) in h

let dflt_handler f desc =
   check_handler ();
   check_bypos ();
   scatter_handler := Some f;
   scatter_desc := desc

let dflt_int ?(min=min_int) ?(max=max_int) (nme: string): (unit -> int) = 
   check_handler ();
   check_bypos ();
   scatter_desc := nme;
   let store = ref None in
   let h s = try 
      if !store <> None then
         failure (fmt "Error: argument '%s' multiply defined\n" nme);
      let v = int_of_string s in
      if v < min then
         failure (fmt "Error: argument '%s' must be greater than %d\n" nme min);
      if v > max then
         failure (fmt "Error: argument '%s' must be less than %d\n" nme max);
      store := Some v
   with _ -> 
      failure (fmt "Error: argument '%s', integer expected, get '%s'\n" nme s);
   in
   let f () = match !store with
      | None ->
         failure (fmt "Error: argument '%s' is mandatory\n" nme)
      | _ -> ()
   in
   scatter_handler := Some h;
   scatter_finalize := f;
   (function () -> match !store with Some v -> v | _ -> assert false)

let dflt_int_option ?(min=min_int) ?(max=max_int) (nme: string): (unit -> int option) = 
   check_handler ();
   check_bypos ();
   scatter_desc := nme;
   let store = ref None in
   let h s = try 
      if !store <> None then
         failure (fmt "Error: argument '%s' multiply defined\n" nme);
      let v = int_of_string s in
      if v < min then
         failure (fmt "Error: argument '%s' must be greater than %d\n" nme min);
      if v > max then
         failure (fmt "Error: argument '%s' must be less than %d\n" nme max);
      store := Some v
   with _ -> 
      failure (fmt "Error: argument '%s', integer expected, get '%s'\n" nme s);
   in
   scatter_handler := Some h;
   (function () -> !store)

let dflt_string (nme: string): (unit -> string) = 
   check_handler ();
   check_bypos ();
   scatter_desc := nme;
   let store = ref None in
   let h s =
      if !store <> None then 
         failure (fmt "Error: argument '%s' multiply defined\n" nme);
      store := Some s
   in
   let f () = match !store with
      | None ->
         failure (fmt "Error: argument '%s' is mandatory\n" nme)
      | _ -> ()
   in
   scatter_handler := Some h;
   scatter_finalize := f;
   (function () -> match !store with Some v -> v | _ -> assert false)

let dflt_string_option (nme: string): (unit -> string option) = 
   check_handler ();
   check_bypos ();
   scatter_desc := nme;
   let store = ref None in
   let h s =
      if !store <> None then 
         failure (fmt "Error: argument '%s' multiply defined\n" nme);
      store := Some s
   in
   scatter_handler := Some h;
   (function () -> !store)

(* OPTIONAL ARGS, e.g.:
   Cmd.arg_set "-quiet" "silent exec"
   Cmd.arg_int "-seed" ~dflt:0 "set random seed"
 *)
let _arg_list: ((string * Arg.spec * string) list) ref = ref []

(* arg unit -> performs a side effet *)
let arg_unit (opt: string) (msg: string) (f:unit->unit) : unit =
   _arg_list := (opt, Arg.Unit f, msg)::(!_arg_list)

let arg_set (opt: string) (msg: string) : (unit -> bool) =
   let store = ref false in
   _arg_list := (opt, Arg.Set store, msg)::(!_arg_list);
   (function () -> !store)

let arg_int (opt: string) ?(dflt=0) ?(min=min_int) ?(max=max_int) (msg:string): (unit -> int) =
   let store = ref dflt in
   let do_store v =
      if v < min then
         failure (fmt "Error: argument of '%s' must be greater than %d\n" opt min);
      if v > max then
         failure (fmt "Error: argument of '%s' must be less than %d\n" opt max);
      store := v
   in
   _arg_list := (opt, Arg.Int do_store, msg)::(!_arg_list);
   (function () -> !store)

let arg_float (opt: string) ?(dflt=0.0) ?(min=0.0) ?(max=max_float) (msg:string): (unit -> float) =
   let store = ref dflt in
   let do_store v =
      if v < min then
         failure (fmt "Error: argument of '%s' must be greater than %.2f\n" opt min);
      if v > max then
         failure (fmt "Error: argument of '%s' must be less than %.2f\n" opt max);
      store := v
   in
   _arg_list := (opt, Arg.Float do_store, msg)::(!_arg_list);
   (function () -> !store)

let arg_int_option (opt: string) ?(min=min_int) ?(max=max_int) (msg:string): (unit -> int option) =
   let store = ref None in
   let do_store v =
      if v < min then
         failure (fmt "Error: argument of '%s' must be greater than %d\n" opt min);
      if v > max then
         failure (fmt "Error: argument of '%s' must be less than %d\n" opt max);
      store := Some v
   in
   _arg_list := (opt, Arg.Int do_store, msg)::(!_arg_list);
   (function () -> !store)

let arg_string (opt: string) ?(dflt="") (msg:string): (unit -> string) =
   let store = ref dflt in
   let do_store v =
      store := v
   in
   _arg_list := (opt, Arg.String do_store, msg)::(!_arg_list);
   (function () -> !store)

let arg_string_option (opt: string) (msg:string): (unit -> string option) =
   let store = ref None in
   let do_store v = store := Some v in
   _arg_list := (opt, Arg.String do_store, msg)::(!_arg_list);
   (function () -> !store)

(* big_int support *)
open Big_int
let optapp (f: 'a -> 'b) (d: 'b) (x:'a option) =
   match x with Some v -> f v | None -> d

let arg_big_int (opt: string) ?(dflt=zero_big_int) ?(min=None) ?(max=None) (msg:string): (unit -> big_int) =
   let store = ref dflt in
   let do_store s =
      try ( 
         let v = big_int_of_string s in 
         let sof x = match x with Some v -> string_of_big_int v | None -> assert false in
         if optapp (fun x -> lt_big_int v x) false min then
            failure (fmt "Error: argument of '%s' must be greater than %s\n" opt (sof min));
         if optapp (fun x -> gt_big_int v x) false max then
            failure (fmt "Error: argument of '%s' must be less than %s\n" opt (sof max));
         store := v
      ) with _ -> failure (fmt "Error: argument of '%s' must be integer (get '%s')\n" opt s)
   in
   _arg_list := (opt, Arg.String do_store, msg)::(!_arg_list);
   (function () -> !store)

let arg_big_int_option (opt: string) ?(min=None) ?(max=None) (msg:string): (unit -> big_int option) =
   let store = ref None in
   let do_store s =
      try ( 
         let v = big_int_of_string s in 
         let sof x = match x with Some v -> string_of_big_int v | None -> assert false in
         if optapp (fun x -> lt_big_int v x) false min then
            failure (fmt "Error: argument of '%s' must be greater than %s\n" opt (sof min));
         if optapp (fun x -> gt_big_int v x) false max then
            failure (fmt "Error: argument of '%s' must be less than %s\n" opt (sof max));
         store := Some v
      ) with _ -> failure (fmt "Error: argument of '%s' must be integer (get '%s')\n" opt s)
   in
   _arg_list := (opt, Arg.String do_store, msg)::(!_arg_list);
   (function () -> !store)


(* mimics the behavior of standard Arg.Symbol:
   - no default value handling
   - handled by side effect (callback)
*)
let arg_symbol (opt: string) (values: string list) (callback: string -> unit) (msg: string): unit = 
   _arg_list := (opt, Arg.Symbol (values, callback), msg)::(!_arg_list)

(* usage *)
let usage_msg () : string =
   P.sprintf "%s version %s\nusage: %s %s <options> | %s -help"
   !toolname !toolversion !toolname !scatter_desc !toolname

let usage (): unit =
   P.fprintf stderr "%s\n" (usage_msg ());
   Arg.usage !_arg_list "";
   exit 0

(* new: dbg arg is a regexp *)
   (* let fl = Verbose.flag_list () in *)
let setdbg re = 
   let fl = Verbose.flag_list () in
   let rg = Str.regexp (re^"$") in
   let dodbg s =
      if Str.string_match rg s 0 then (
         (* P.printf "%s\n" s *)
         let x = V.get_flag s in
         V.set_flag x
      ) 
   in
   List.iter dodbg fl

(* generic md formating *)
let title underline s =
   let l = String.length s in
   let ul = String.make l underline in
   Printf.sprintf "\n%s\n%s\n\n%!" s ul
let bold s = "**"^s^"**"

(* markdown to man
pandoc -s -f markdown -t man man-template.md | groff -T utf8 -man | less
*)

(* MANUAL *)
let opts = ref []
let md_man (os: out_channel)  = (
   let pr = Printf.fprintf in
   pr os "%% %s(1) Version %s\n%!"
      (String.uppercase_ascii !toolname)
      !toolversion
   ;
   pr os "%s" (title '=' "NAME");
   pr os "%s — %s\n" (bold !toolname) !toolgoal;
   pr os "%s" (title '=' "SYNOPSIS");
   pr os "| %s %s <options>\n| %s -help\n"
      (bold !toolname) !scatter_desc (bold !toolname);
   if !tooldescription <> "" then
      pr os "%s" (title '=' "DESCRIPTION");
      pr os "%s\n" !tooldescription;
   pr os "%s" (title '-' "OPTIONS");
   !opts |> List.iter (fun (f,_,c) ->
      pr os "%s\n\n" f;
      pr os ": %s\n\n" c;
   );
)

(* RUN THE COMMAND *)
let run (main: unit -> unit) : unit = try (
   let do_others = match !scatter_handler with
      | Some h -> h
      | None -> make_bypos_handler ()
   in
   (* predef args: verbose and dbg flags support *)
   let dbgflags = U.string_of_string_list
      ~obr:"{" ~cbr:"}" ~sep:"|" (V.flag_list ())
   in
   opts := (List.rev !_arg_list)@
      [
         ("-v", Arg.Unit (fun () ->  V.set 1), ": Set verbose mode");
         ("-vv", Arg.Unit (fun () ->  V.set 2), ": More verbose mode");
         (* ("-dbg", Arg.Symbol (dbgflags, setdbg), ": set debug flag"); *)
         ("-dbg", Arg.String setdbg, dbgflags^" : Set debug flag");
         ("-man", Arg.Unit (fun () -> md_man stdout; exit 0), ": Print short manual");
      ]
   ;
   Arg.parse !opts do_others (usage_msg ());
   (!scatter_finalize) ();
   main ()
) with Cmd_failure msg -> (
   P.fprintf stderr "%s\n" (usage_msg ());
   P.fprintf stderr "%s\n" msg;
   exit 1
)

