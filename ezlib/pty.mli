(*----------------------------------------------------------------------
	module : Pty
	date :
------------------------------------------------------------------------

----------------------------------------------------------------------*)

type t

val make:  out_channel -> t 

val cr: t -> unit
val tab: t -> unit
val untab: t -> unit

val prints: t -> string -> unit
val printf: t -> ('a, unit, string, unit) format4 -> 'a
