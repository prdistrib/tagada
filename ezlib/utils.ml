(* Set utilities *)

(* ====== PRINTABLE FUNCTIONAL TYPES UTILS =====*) 
(* Signature: ordered and printable types *)
module type XType =
   sig
      type t
      val compare: t -> t -> int
      val to_string: t -> string
      val output: out_channel -> t -> unit
   end;;

(* MODULE: ORDERED and PRINTABLE INT
IMPORTANT => export t=int in the signature !
*)
module XInt : XType with type t=int =
  struct
    type t = int
    let compare = Stdlib.compare
    let to_string = Stdlib.string_of_int
    let output os x = to_string x |> output_string os
  end;;


(* MODULE: ORDERED and PRINTABLE STRING *)
module XString : XType with type t=string =
  struct
    type t = string
    let compare = Stdlib.compare
    let to_string = fun x -> x
    let output = output_string
  end;;

(* FUNCTOR: PAIRS *)
module XPair (Elt: XType) =
   struct
      type t = Elt.t * Elt.t
      let compare = Stdlib.compare
      let to_string: t -> string = fun (x,y) ->
         "("^(Elt.to_string x)^","^(Elt.to_string y)^")"
      let output os (x,y) =
         Printf.fprintf os "(%a,%a)" Elt.output x Elt.output y
   end;;

(* FUNCTOR: LIST *)
module XList (Elt: XType) =
   struct
      type t = Elt.t list
      let compare = Stdlib.compare
      let to_string: t -> string = fun x ->
         "["^(String.concat ", " (List.map Elt.to_string x)) ^"]"
      let output (os: out_channel) (x: t) : unit =
         Printf.fprintf os "[%a]"
            (fun os x -> List.iteri
               (fun i e ->
                  Printf.fprintf os "%s%a"
                  (if (i=0) then "" else "; ")
                  Elt.output e
               )
               x
            )
            x
   end;;

module IntList = XList(XInt)

(* FUNCTOR: ARRAY *)
module XArray (Elt: XType) =
   struct
      type t = Elt.t array
      let compare = Stdlib.compare
      let make = Array.make
      let to_string: t -> string = fun x ->
         "[|"^(String.concat "; " (Array.to_list @@ Array.map Elt.to_string x)) ^"|]"

      let output (os: out_channel) (x: t) : unit =
         Printf.fprintf os "[|%a|]"
            (fun os x -> Seq.iteri
               (fun i e ->
                  Printf.fprintf os "%s%a"
                  (if (i=0) then "" else "; ")
                  Elt.output e
               )
               x
            )
            (Array.to_seq x)
   end;;

module XXArray (Elt: XType) =
struct
   include Array
   type t = Elt.t Array.t
      let to_string: t -> string = fun x ->
         "[|"^(String.concat "; " (Array.to_list @@ Array.map Elt.to_string x)) ^"|]"

      let output (os: out_channel) (x: t) : unit =
         Printf.fprintf os "[|%a|]"
            (fun os x -> Seq.iteri
               (fun i e ->
                  Printf.fprintf os "%s%a"
                  (if (i=0) then "" else "; ")
                  Elt.output e
               )
               x
            )
            (Array.to_seq x)
end;;

module IntArray = XArray(XInt)
module IntXArray = XXArray(XInt)

(* FUNCTOR: SETS OVER ORDERED and PRINTABLE TYPES *)
module XSet (Elt: XType) =
   struct
      include Set.Make (
         struct
            type t = Elt.t
            let compare = Elt.compare
         end
      ) 
      let to_string: t -> string = fun x ->
         "{"^(String.concat ", " (List.map Elt.to_string (elements x))) ^"}"

      let output (os: out_channel) (x: t) : unit =
         Printf.fprintf os "{%a}"
            (fun os x -> Seq.iteri
               (fun i e ->
                  Printf.fprintf os "%s%a"
                  (if (i=0) then "" else ", ")
                  Elt.output e
               )
               x
            )
            (to_seq x)
(* ICI: should be removed *)
      let of_list l =
         List.fold_left (fun s x -> add x s) empty l
      let of_array a =
         Array.fold_left (fun s x -> add x s) empty a
      let of_maped_list f l =
         List.fold_left (fun s x -> add (f x) s) empty l
   end;;

(* some usefull sets *)
module IntSet = struct
   include XSet (XInt)
   (* range [first,last] *)
   let of_range first last =
      assert (first <= last);
      let nb = 1+last-first in
      let eseq = Seq.init nb (fun i -> first+i) in
      of_seq eseq
end

module StringSet = XSet (XString)

(* FUNCTOR: MAPS OVER ORDERED and PRINTABLE TYPES *)
module XMap (Key: XType) (Val: XType) =
   struct
      module KM = Map.Make (
         struct
            type t = Key.t
            let compare = Key.compare
         end
      )
      (* include Map.Make ( struct type t = Key.t let compare = Key.compare end) *)
      include KM
      type t = Val.t KM.t 
      (* type t = Val.t t *)
      let to_string: t -> string = fun x ->
         let bs (k, v) = "("^(Key.to_string k)^" -> "^(Val.to_string v)^")" in
         "{"^(String.concat ", " (List.map bs (bindings x))) ^"}"

      let output (os: out_channel) (x: t) =
         let outbinding os (k,v) =
            Printf.fprintf os "(%a -> %a)"
               Key.output k Val.output v
         in
         Printf.fprintf os "{%a}"
            (fun os x -> Seq.iteri
               (fun i b ->
                  Printf.fprintf os "%s\n%a"
                  (if (i=0) then "" else ", ")
                  outbinding b
               )
               x
            )
            (to_seq x)
(* ICI: sould be removed *)
      let of_list: (Key.t * Val.t) list -> t = fun lst ->
         List.fold_left (fun a (k,v) -> add k v a) empty lst 
   end
;;
         
(* some usefull maps *)
module Int2String = XMap (XInt) (XString)
module Int2Int = XMap (XInt) (XInt)
module Int2IntSet = XMap (XInt) (IntSet)


(* ====== MISC PRINTERS UTILS =====*) 
(* WARNING, partially obsolete:
  - use the X... module mechanism with 'output' and %a in format 
   (whenever possible)
  - use generic output_list otherwise
*)

let string_of_list
   ?(obr: string = "[")
   ?(cbr: string = "]")
   ?(sep: string = "; ")
   (soa: 'a -> string) (al: 'a list) = 
   obr^(String.concat sep (List.map soa al))^cbr

let output_list 
   ?(obr: string = "[")
   ?(cbr: string = "]")
   ?(sep: string = "; ")
   (soa: 'a -> string)
= fun (os: out_channel) (al: 'a list) -> (
   output_string os (string_of_list ~obr ~cbr ~sep soa al)
) 

let string_of_string_list
   ?(obr: string = "[")
   ?(cbr: string = "]")
   ?(sep: string = "; ")
   sl = string_of_list ~obr:obr ~cbr:cbr  ~sep:sep (fun x->x) sl

let string_of_int_list
   ?(obr: string = "[")
   ?(cbr: string = "]")
   ?(sep: string = "; ")
   sl = string_of_list ~obr:obr ~cbr:cbr ~sep:sep string_of_int sl

let string_of_string_list_list
   ?(obr1: string = "[")
   ?(cbr1: string = "]")
   ?(sep1: string = "; ")
   ?(obr2: string = "[")
   ?(cbr2: string = "]")
   ?(sep2: string = "; ")
   sll =
   string_of_list ~obr:obr1 ~cbr:cbr1 ~sep:sep1
      (string_of_string_list ~obr:obr2 ~cbr:cbr2 ~sep:sep2) sll

let rec string_of_assocs = function
   (k,v)::[] -> k^"="^v
|  (k,v)::t -> k^"="^v^" "^(string_of_assocs t)
|  [] -> ""

let idy x = x

let string_of_option (soa: 'a -> string) (ao: 'a option) = 
   match ao with
   | None -> "None"
   | Some a -> "Some("^(soa a)^")" 

(* more general *)
let opt (d: 'a) (x: 'a option) : 'a =
   match x with
   | None -> d
   | Some a -> a

(* ====== MISC HASH UTILS =====*) 
let string_of_hashtab (sok: 'a -> string) (soe: 'b -> string) (abtbl: ('a,'b) Hashtbl.t) =
   let soab k v acc = ((sok k)^" -> "^(soe v)^";")^acc in
   "["^(Hashtbl.fold soab abtbl "")^"]"

let dump_hashtab ?(oc=stderr) (sok: 'a -> string) (soe: 'b -> string) (abtbl: ('a,'b) Hashtbl.t) =
   let dab k v = Printf.fprintf oc "%s -> %s;\n" (sok k) (soe v) in
   Hashtbl.iter dab abtbl

let hash_content ht =
   Hashtbl.fold (fun k v a -> (k,v)::a) ht []

let sorted_hash_content ht =
   let c = hash_content ht in
   List.fast_sort Stdlib.compare c

(* foo to bar list hashtables *) 
(* get elt in 'key -> val list' Hashtbl
   returns [] if Not_found 
*)
let find_hash_list (tab: ('a, 'b list) Hashtbl.t) (k: 'a) : 'b list =
try Hashtbl.find tab k with Not_found -> []

(* add elt in 'key -> val list' Hashtbl *)
let add_hash_list (tab: ('a, 'b list) Hashtbl.t) (k: 'a) (v: 'b) =
   let oldv = try
      Hashtbl.find tab k
   with Not_found -> []
   in
   Hashtbl.replace tab k (v::oldv)

(* same sort the list *)
let add_hash_sorted_list (cmp: 'b -> 'b -> int) (tab: ('a, 'b list) Hashtbl.t) (k: 'a) (v: 'b) =
   let oldv = try
      Hashtbl.find tab k
   with Not_found -> []
   in
   Hashtbl.replace tab k (List.fast_sort cmp (v::oldv))


(* JUST A TEST
type toto = int * string
module TotoSet = Set.Make (
   struct
      type t = toto
      let compare = Stdlib.compare
   end
)
*)

let rec iter_sep (f: 'a -> unit) (g: unit -> unit) (l: 'a list) : unit =
   match l with
   | [] -> ()
   | [x] -> f x
   | x::l' -> f x; g (); iter_sep f g l'

let iteri_sep (f: int -> 'a -> unit) (g: unit -> unit) (l: 'a list) : unit =
   let rec _iteri i l =
      match l with
      | [] -> ()
      | [x] -> f i x
      | x::l' -> f i x; g (); _iteri (i+1) l'
   in
   _iteri 0 l

let rec make_list (i: int) (x:'a) : 'a list =
   if i <= 0 then [] else x::(make_list (i-1) x)

(* ====== MISC STDERR UTILS =====*) 
exception Fatal_error of string
let raise_error fmt =
   let f s = raise (Fatal_error s) in
   Printf.ksprintf f fmt

let warning fmt =
   let f s = Printf.fprintf stderr "Warning: %s" s in
   Printf.ksprintf f fmt



(* PHYSICAL-IDENTITY HASH TABLE
usage example:

module Stbl = PhyHtbl (
   struct
      type t = string 
   end
);;
*)

module type PhyKey =
sig
   type t
end;;

module PhyHtbl (K: PhyKey) =
struct
   let phy_hash (x:'a) = (
      if Obj.is_block (Obj.repr x) then
         Hashtbl.hash (Nativeint.of_int (Obj.magic x)) (* magic *)
      else
         invalid_arg "Can only find address of boxed values."
   )
   include Hashtbl.Make(
      struct
         type t = K.t
         let equal = (==)
         let hash = phy_hash
      end
   )
end;;


(* multi-set as ordered list of ('a * int) *)

let multi_set_of_list (cmp: 'a -> 'a -> int) (l: 'a list) : ('a * int) list =
   let sl  = List.fast_sort cmp l in
   let r = sl |> List.fold_left (fun acc e ->
      match acc with
      | (x,n)::r when (cmp x e) = 0 -> (x,n+1)::r
      | _ -> (e,1)::acc
   ) []
   in 
   List.rev r
