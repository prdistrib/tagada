
(* $Id$ *)

(** Quickly write a command-line tool with arguments and options

   - V. 1.1
   - Provides an abstract/concise interface over Arg. 
   - Handles some predefined options (requires Ez): verbose, debug
   - generates a (basic) user manual 
   
   Usage:
   {ol
   {- customize if needed (tool name, version)}
   {- Declare all needed arguments and/or options}
   {- Run}
   }

   Requires {!Ez.Verbose}

   Predefined options:
   - [-h], [--help], print usage and stops
   - [-v], [-vv], set verbose mode 1 or 2 (see {!Verbose} module)
   - [-dbg "pattern"]
      {ul 
         {- Sets any flag GLOBALLY declared with [Verbose.get_flag] that
         matches [Str.reg_exp "pattern"]}
         {- e.g., [-dbg "foo"] sets the flag created with [Verbode.get_flag "foo"] (if any)}
         {- e.g., [-dbg ".*"] sets all flags }
      }

   TODO:
   - better handling of params:
   - e.g. allow multiple optionnal param
     if they can be distinguished lexically ?
*)

(** {1 Name, version, description of the tool} *)

val set_name: string -> unit
(** Default Sys.argv.(0) *)

val set_version: string -> unit
(** Default "0.0" *)

val set_goal: string -> unit
(** (Very) short description, for 'usage' *)

val set_description: string -> unit
(** (Possibly) long description for man *)

val tool_name : unit -> string
val tool_version : unit -> string

val md_man : out_channel -> unit
(** Print man on os, with markdown formating *)

(** {1 UNFLAGGED ARGS (aka PARAMS)} *)
(**
   - They are not introduced by a '-xxx' flag.
   - Use ONE OF the following method:
      {ul
      {- declare an external dflt_handler (cf. ocaml Arg lib)}
      {- use a pedefined SINGLE param handler (maybe OPTIONAL)}
      {- declare a list of BY POSITION params (all MANDATORY)
      position is determined by the ordre of declaration}}
*)

val dflt_handler: (string -> unit) -> string -> unit
(** Declare a user defined handler,  *)

val dflt_int_option: ?min:int -> ?max:int -> string -> (unit -> int option)
(** Single-optional int *)

val dflt_string_option: string -> (unit -> string option)
(** Single-optional string *)

val bypos_int: ?min:int -> ?max:int -> string -> (unit -> int) 
val bypos_string: string -> (unit -> string) 
(** By-position MANDATORY params list.
   Params must appear in the command line in order.
*)

val dflt_int: ?min:int -> ?max:int -> string -> (unit -> int)
val dflt_string: string -> (unit -> string)
(** OBSOLETE: SINGLE-MANDATORY (backward compat)
   equivalent to use a single instance of bypos_int or bypos_string
*)

(** {1 FLAGGED ARGS (aka options)} *)

val arg_unit: string -> string -> (unit -> unit) -> unit
(** Side-effect handler, e.g.:
   {v let _ = Cmd.arg_set "-quiet" "silent exec" v}
*)

val arg_set: string -> string -> (unit -> bool)
(** Pure flag, e.g.:
   {v let debug = arg_set "-d" "Set debug mode" v} 
Then acces with:
   {v if debug () then ... v}
*)

open Big_int

(** {2 OPTIONAL NUMBER WITH DEFAULT value} *)

val arg_int:
string (** flag, e.g. "-foo" *)
-> ?dflt:int -> ?min:int -> ?max:int -> string -> (unit -> int)
(** e.g. [let foo = arg_int "-m" ~dflt:1 ~min:1 "get some int value"]
   - if [-m 42] appears in the command-line [foo ()] returns [42],
   - if [-m 0] appears in the command-line, raises an error (cause 0 < min=1)
   - if [-m] does not appear, [foo ()] returns the specified [dflt] (1)
   - if NO [dflt] WAS SPECIFIED, [foo ()] returns [0] (the "default" [dflt])
*)

val arg_string: string -> ?dflt:string -> string -> (unit -> string)

val arg_float: string -> ?dflt:float -> ?min:float -> ?max:float -> string -> (unit -> float)
(** Similar to arg_int; default [dflt] is 0.0 *)

val arg_big_int: string -> ?dflt:big_int -> ?min:big_int option -> ?max:big_int option -> string -> (unit -> big_int)
(** Similar to arg_int; default [dflt] is Big_int.zero *)

(** {2 OPTIONAL NUMBER WITHOUT DEFAULT value} *)

val arg_int_option: string -> ?min:int -> ?max:int -> string -> (unit -> int option)
(** 
   [arg_int_option "-flag" "description"] returns:
   - [fun () -> None] when [-flag] is not present in the command-line
   - otherwise [fun () -> Some v] where [v] is the value that would have
     been returned by using [arg_int].
*)

val arg_big_int_option: string -> ?min:big_int option -> ?max:big_int option -> string -> (unit -> big_int option)
(** Similar to arg_int_option *)

(** TODO arg_float_option *)

(** {2 OPTIONAL STRINGS} *)

val arg_string_option: string -> string -> (unit -> string option)
(** Similar to [arg_int_option]
   TODO define a more general feature with 'integrity' checks, e.g.:
   - must be a proper file name
   - must belong to a list
   - must match a reg-exp etc.
*)

val arg_symbol: string -> string list -> (string -> unit) -> string -> unit
(** Mimics the behavior of the standard Arg.Symbol:
   - no default value handling
   - handled by side effect (callback)
   TODO should be merged in a general 'arg_string' 
*)


(** {1 USAGE} *)

val usage_msg: unit -> string
(** Get the usage message (corresponding to the params/opts declared so far) *)

val usage: unit -> unit
(** Print usage and STOPS *) 

(** {1 RUN THE TOOL} *)

val run: (unit -> unit) ->  unit
(** [run main]
   - Finalizes the list of params/opts declared so far,
   - Parse the command line (Sys.argv) accordingly,
   - Run the 'main' fonction
*)
