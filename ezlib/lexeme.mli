(*
New, april 2020:
 Added a 'lexeme store' mecanism,
 use it preferably to the old 'srcflag' mecanism,
*)

(*** USER SIDE ***)

val new_line : Lexing.lexbuf -> unit

type t = { str : string ; line : int ; cstart : int ; cend : int }

val dummy : t

(* standardized pty *)
val details : t -> string

(* OLD 'srcflaged' STYLE *)
type 'a srcflaged = { src : t ; it  : 'a }

(* NEW store STYLE *) 
type store
val store_get: store  -> string -> t


(*** PARSERS SIDE ****) 

(* NEW store STYLE *) 
val new_store : unit -> store
val store_add : store -> t -> string


(* OLD 'srcflaged' STYLE *)
val flagit : 'a -> t -> 'a srcflaged

(* keep 'make' for compatibility *)
val make : ?offset:int -> Lexing.lexbuf -> t
val of_lexbuf : ?offset:int -> Lexing.lexbuf -> t

(* make lexeme char by char, in an abstract buffer *)
module Buff :
sig
   type t
   (* start to bufferize at lexbuf pos *)
   val create : Lexing.lexbuf -> t
   (* concat on char *)
   val add_char : t -> char -> unit
   (* concat content of lexbuff *)
   val add_lexbuf : t -> Lexing.lexbuf -> unit
end

(* finalize buffer at lexbuf pos *)
val of_buff : Buff.t -> Lexing.lexbuf -> t

val last_made : unit -> t

(* modify the associated string *)
val change_str: t -> string -> t

