
(* $Id$ *)

(** Utilities for invoking bash-command-line

   - Relies only on module Sys (not Unix),
   - BUT uses unix (posix) i/o redirections,
   -> Thus works on linux or cygwin BUT NOT windows
   - In all cases, some result is expected on stdout, and
     this result is stored according to its expected type.
   - Non-0 exit flags causes exection Command_failed

*)

(** {1 Exceptions } *)

exception Command_failed of string * int
(** Raised when system command returns non-0 *)

exception Bad_result of string
(** Raised when the result (stdout) is unexpected *)

(** {1 Execute and store stdout somehow/somewhere} *)

val cmd_to_file : string -> string -> unit 
(** [cmd_to_file c f] (literally) executes "c > f" *)

val cmd_to_string_list : string -> string list
(** To be used when a list of line is expected on stdout *)

val cmd_to_string : string -> string
(** To be used when a single line is expected on stdout *)

val cmd_to_int : string -> int 
(** To be used when a SINGLE LINE, containing a SINGLE integer is expected on stdout *)

val cmd_to_int_list : string -> int list
(** To be used when SEVERAL LINES, each containing a SINGLE integer, is expected on stdout *)


(** {1 Common BASH commands} *)

val date : unit -> int * int * int
(** [date ()] returns (year,month,day) *)

val cat : out_channel -> string -> unit
(** [cat oc fn] flushes the containt of [fn] on [oc] *)
