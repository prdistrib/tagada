
(*

Une interface minimale pour lancer des commandes shell depuis ocaml.

On n'utilise que la module Sys (pas Unix) en esp�rant que ce soit plus portable.

Par contre, on suppose qu'on travaille avec bash (ca devrait donc marcher sous linux ou cygwin ...)

*)

exception Command_failed of string * int
exception Bad_result of string

let cmd_to_file c f =
	let cmd = c^" > "^f in
   let res = Sys.command cmd in
   match res with
   | 0 -> ()
   | _ -> raise (Command_failed (c,res))
      

let cmd_to_string_list c = (
	let tmpfile = Filename.temp_file "Ezshell" ".res" in
	let cmd = c^" > "^tmpfile in
   let res = Sys.command cmd in
   if (res = 0) then (
		(* on renvoie une liste de strings : une ligne = une string *)
		let ins = open_in tmpfile in
		let rec makell () = (
			try (
				let l = input_line ins in
				l :: (makell ())
			) with End_of_file -> (
				[]
			)
		) in
		let ll = makell ()  in
      close_in_noerr ins;
		let _ = try Sys.remove tmpfile with _ -> () in
		ll
	) else (
		let _ = try Sys.remove tmpfile with _ -> () in
		raise (Command_failed (c,res))
   )
)

let cmd_to_string c = (
	match (cmd_to_string_list c) with
	[ s ] -> s
	| l -> (
		let msg = Printf.sprintf
		          "cmd_to_string: 1 line expected but get %d line(s)"
		             (List.length l) 
		in
		raise (Bad_result msg)
	)
)

let cmd_to_int c = (
	match (cmd_to_string_list c) with
	[ s ] -> (
		try (
			int_of_string s
		) with _ -> (
			let msg =
				Printf.sprintf "cmd_to_int: int expected but get \"%s\"" s in
			raise (Bad_result msg)
		)
	) | l -> (
		let msg =
			Printf.sprintf "cmd_to_int: 1 line expected but get %d line(s)" (List.length l) in
		raise (Bad_result msg)
	)
)

let cmd_to_int_list c = (
	let l = cmd_to_string_list c in
	let f s = (
		try (
			int_of_string s
		) with _ -> (
			let msg =
				Printf.sprintf "cmd_to_int_list: int expected but get \"%s\"" s in
			raise (Bad_result msg)
		)
	) in
	List.map f l
)

(*----------------------------
Quelques commandes utiles ...
----------------------------*)

let date () = (
	match ( cmd_to_int_list "date +%Y%n%m%n%d" ) with 
	[ y;m;d ] -> (y,m,d)
	| _ -> (
		let msg =
			Printf.sprintf "date: unexpected res" in
		raise (Bad_result msg)
	)
)

let cat oc fn =
   let ic = try
      open_in fn
   with _ ->
      Printf.fprintf stderr "Ezbash.cat error: file '%s' doest not exist\n" fn;
      exit 1
   in try (
      while true do
         let s = input_line ic in
         output_string oc s;
         output_string oc "\n";
      done  
   ) with End_of_file -> (close_in_noerr ic) 

