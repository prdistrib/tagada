(*
Un module au dessus de lexer et parser
pour �viter les d�pendances boucl�es
*)

(* open Lexing *)
(* open Printf *)

(* le type ``lexeme'', string + info source *)
type t = { str : string ; line : int ; cstart : int ; cend : int }
let dummy = { str = "dummy" ; line = 0 ; cstart = 0 ; cend = 0 }

(* new 'store' style, april 2020 *)
module HH = Utils.PhyHtbl (
struct
   type t = string
end
)
type store = t HH.t
let new_store () = HH.create 512
let store_add (tab: store) (x: t) : string = 
   HH.add tab x.str x; x.str
let store_get (tab: store) (s: string) =
   try HH.find tab s with Not_found -> dummy

(* pour calculer line/col *)
let line_num = ref 1
let line_start_pos = ref 0

let new_line ( lexbuf ) = (
	line_start_pos := Lexing.lexeme_end lexbuf;
	incr line_num;
   Verbose.printf ~l:2 "\rreading line %4d" !line_num;
(* printf "making newline\n"; flush stdout ; *)
	()
)

let change_str x s = { x with str = s}

(* constructeur de type flagg� avec un lexeme *)
type 'a srcflaged = {
   src : t ;
   it  : 'a
}
(* flagage d'une valeur quelconque *) 
let (flagit : 'a -> t -> 'a srcflaged) = 
function x -> function lxm -> 
{ it = x; src = lxm }


let last_lexeme = ref dummy 

let make ?(offset:int=0) (lexbuf) = (
   let s = (Lexing.lexeme lexbuf) in
   let s = if offset > 0
            then String.sub s offset ((String.length s) -offset)
            else s
   in
   let l = !line_num in
   let c1 = (Lexing.lexeme_start lexbuf - !line_start_pos + 1 + offset) in
   let c2 = (Lexing.lexeme_end lexbuf - !line_start_pos) in
      last_lexeme := { str = s ; line = l; cstart = c1 ; cend = c2 };
      (* printf "making lexeme '%s'\n" s; flush stdout; *)
      !last_lexeme
)
let of_lexbuf = make

(* build lexeme in buffer, char by char, usefull e.g. to parse literal strings *)
module Buff = struct
   type t = int * int * Buffer.t
   let create lexbuf = (!line_num, (Lexing.lexeme_start lexbuf - !line_start_pos + 1), Buffer.create 128)
   let add_char (_,_,buf) ch = Buffer.add_char buf ch
   let add_lexbuf (_,_,buf) lexbuf = Buffer.add_string buf (Lexing.lexeme lexbuf)
end

let of_buff (l,c1,buf) lexbuf = 
   let c2 = (Lexing.lexeme_end lexbuf - !line_start_pos) in
   last_lexeme := { str = Buffer.contents buf;  line = l; cstart = c1 ; cend = c2 };
   !last_lexeme

let last_made () = !last_lexeme 

let details lxm =
    Printf.sprintf "'%s' (line %d, char %d to %d)" lxm.str lxm.line lxm.cstart lxm.cend

