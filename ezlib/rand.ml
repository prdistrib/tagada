(* some utils in top of Random *)

(* inherits all Random's stuff *)
include Random
module V = Verbose

let dbg = V.get_flag "rnd"

(* pseudo-Gaussian centered on 0, dev 1 *)
let iof = int_of_float
let foi = float_of_int
let pi = 4. *. atan 1.
let gaussian () =
  sqrt (-2. *. log (float 1.)) *. cos (2. *. pi *. float 1.)

let int_around m =
   let rg = gaussian () in
   iof ((foi m) *. (0.2 *. rg +. 1.))

let int_between i j = try (
   i + (int (j-i+1))
   ) with x -> (
      V.xprintf ~f:dbg "int_between %d %d\n" i j; 
      raise x
   )

(* p is 0..100 
   return true with a proba p/100
   false otherwise
*)
let percent p = try (
      p > Random.int 100 
   ) with x -> (
      V.xprintf ~f:dbg "percent %d\n" p; 
      raise x
   )
   
(* auto_seed:
   radomly pick a seed, 
   init with this seed
   return the seed 
n.b. Random.int is limited to 2^30-1
*) 
let max_seed = ((1 lsl 30) -1)

let auto_seed () =
   self_init ();
   let seed = int max_seed in
   init seed;
   seed


