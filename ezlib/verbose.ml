(*----------------------------------------------------------------------
	module : Verbose
	date :
------------------------------------------------------------------------
-- New 2022/11: 'why' system (verbose prefix string)
-- New 2010/11: debug flag system
-- New 2016/02:
   add the "os" feature
   (just the principle for the moment since os=stderr !)
-- New 2016: corrected flag system
   whetever is the level, a verbose called with a flag
   is exec/printed IFF the flag is set

---- probably obsolete, two original versions:

N.B. �a a l'air de marcher, seul probl�me c'est peut-�tre
     l'efficacit�, vu que la string '"format" args...' est
     �valu�e m�me si on n'est pas en mode verbose ...
     Autrement dit : c'est l'affichage qui est 'bloqu�'
     en non-verbose, pas l'�valuation du message ... 

N.B. VERSION GORE : le kprintf n'est appel� que si besoin,
     sinon c'est une branche "gore" qui est appel�e :
     - evalue le nb d'args attendu ****ATTENTION = nbre de % !!!
     - "d�pile" ces nb args sans rien en faire

----------------------------------------------------------------------*)

module P=Printf

let _os = ref stderr
let os () = !_os

(* FLAG SYSTEM *)
type _flag = {
   mutable fval: bool;
   fname: string
}
type flag = _flag option

let _no_flag:flag = None

let _flag_tab : (string, flag) Hashtbl.t = Hashtbl.create 10
(* warning: last in first ! *)
let _flag_list : string list ref = ref []

let get_flag s = (
(* P.printf "get_flag %s\n" s; *)
	try (
		Hashtbl.find _flag_tab s
	) with Not_found -> (
		let res = Some {fval=false; fname=s} in
		Hashtbl.add _flag_tab s res;
		_flag_list := s::!_flag_list;
		res
	) 
)

let set_flag = function
   | Some f -> f.fval <- true
   (* unreachable since flags are necessarily returned by 'get_flag' *)
   | None -> assert false

let flag_list () = !_flag_list

(* type msg = string Lazy.t *)

let _level = ref 0

let on () = ( _level := 1 )
let off () = ( _level := 0 )
let set (l:int) = ( _level := l )

(* no longer used, should be an optional argument of the others functions *)
(* let level () = !_level *)

let _doit (l:int) (f:flag): bool = match f with
   | Some x -> x.fval
   | None -> (!_level >= l)

(* WHY/PREFIX SYSTEM:
   A string to print at the begining, default:
      "[v]" for _level=1
      "[v%d] l" for _level>1
      "[%s] f" for flag f
   It is determined 'dynamically' according to the args l and f 
*)
let _why_context = ref (0,None)

let _inherit_why l2 f2 =
   let (l1,f1) = !_why_context in
   match (l2, f2) with
      | (_, Some _) -> (l1, f2)
      | _ -> (max l1 l2, f1)

let _why_string (l:int) (f:flag): string = 
   let (al, af) = _inherit_why l f in
   match af with
   | Some x -> "["^x.fname^"] "
   | None -> (
      match al with
      | 0 -> ""
      | 1 -> "[v] "
      | _ -> "["^(string_of_int al)^"] "
   )

(**** VERSION PAS TROP GORE *****)
let printf ?(l=1) ?(f=_no_flag) s = (
	P.kprintf (fun t -> if (_doit l f) then (prerr_string t; flush stderr) else ()) s 
)
let print_string ?(l=1) ?(f=_no_flag) s =
   if (_doit l f) then prerr_string s else ()

(* why system within 'exe':
   WARNING do not nest 'exe', 
*)
let _exe_why = ref ""
let why () = prerr_string (!_exe_why); flush stderr
let exe ?(l=1) ?(f=_no_flag) (func: unit -> unit) : unit = (
	if (_doit l f) then (
      assert(!_exe_why = "");
      _exe_why := _why_string l f;
      func ();
      _exe_why := ""
   ) else ()
)
let get_why_string l f = (
   match !_exe_why with
   | "" -> _why_string l f
   | s -> s
)
let xprintf ?(l=1) ?(f=_no_flag) s = (
   let doit s = if (_doit l f) then (
      let ws = get_why_string l f in
(* ICI *)
(* P.fprintf stderr "Verbose.xprintf: get_why_string = %s\n%!" ws; *)
         prerr_string ws;
         prerr_string s;
         flush stderr
      ) else ()
   in
	P.kprintf doit s
)
let xprint_string ?(l=1) ?(f=_no_flag) s =
   if (_doit l f) then (
      prerr_string (get_why_string l f);
      prerr_string s
   ) else ()


(**** VERSION GORE *****)
(*
let count_args s = (
	let max = (String.length s) -1  in
	let res = ref 0 in
	for i = 0 to max do
		if ((String.unsafe_get s i) = '%') 
		then (incr res)
		else ()
	done;
	!res
) 

let put s = (
	if (!_level > 0) then (
		(* let toto = string_of_format s in *)
		(* print_string toto ; flush stdout; *)
		P.kprintf (fun t -> prerr_string t) s 
	) else (
		let toto = string_of_format s in
		(* print_string toto ; flush stdout; *)
		let rec f n x = (
		P.printf "appel de f %d\n" n; flush stdout;
			if (n > 0) then (
				Obj.magic (f (n-1))
			) else (Obj.magic ())
		) in
		let nbargs = count_args toto in 
		Obj.magic ( f nbargs)
	)
)
*)

(* put "%d %s %d\n" 42 "toto" 43; flush stderr;; *)
