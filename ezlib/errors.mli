
(*----------------------------------------------------------------------

WARNING: old module for handling errors in a tool
   (typically a compiler) that uses the Lexeme module
For a more flexible/less ad-hoc solution, use the
   Verbose error and warning feature


	module : Errors.mli
	date :
------------------------------------------------------------------------

	Exceptions et formatage des messages d'erreur.

	La compilation peut �chouer sur des exceptions pr�d�finies :

	- Sys_error of string :
	-----------------------
	erreur syst�me (li�e aux fichiers)

	- Parse_error :
	---------------
	dans le yacc

	- Compile_error of Lexeme.t * string :
	--------------------------------------
	une erreur de compil qu'on peut "situer" dans le source gr�ce
	� un lexeme *

	- Global_error of string :
	--------------------------
	une erreur qui n'est pas sp�cialement li�e � un lexeme,
	(mais peut-�tre li�e � un noeud -> void checkNode) *

	- Compile_node_error of CompileData.node_key * Lexeme.t * string :
	------------------------------------------------------------------
	une erreur de compil li�e � un lexeme et � un noeud (en fait
	une instance de template) particulier * ** ***

	- Global_node_error of CompileData.node_key * string :
	------------------------------------------------------------------
	une erreur de compil non li�e � un lexeme particulier,
	mais dependant lev�e lors de la compilation d'un noeud * ** ***

	* Les diff�rentes phases peuvent �ventuellement "capter"
	locallement les exceptions pr�c�dentes (histoire de sortir
	plus d'un message d'erreur par compilation).

	** Les erreurs li�es � un noeuds sont g�n�r�es UNIQUEMENT
	au niveau de checkNode => lors de la compil d'un noeud,
	les sous-proc�dures se contentent de lever des erreurs
	``simples'' (Global ou Compile), qui sont capt�es au niveau
	de checkNode o� on ``rajoute'' l'info sur le noeud
	courant (�a �vite de toujours passer en param�tre  le noeud courant).

	*** Pas d�fini ici (� cause des %@#&! problemes de compil s�par�e),
	voir Compile_data


----------------------------------------------------------------------
A titre indicatif, voici la liste des messages d'erreurs (actuels)
- Au lancement/chargement :

"can't load main node: <name> is undefined"
"can't load main node: <name> is a function"
"can't load main node: <name> is a template"

- Au cours du static check (toutes sont associ�es � un lexeme) :

"can't eval constant: <msg>"
"can't eval type: <msg>"

"recursion loop detected: <recursion path>"
"unbound type"
"undefined constant or variable"
"undefined node or function"
"undefined type"
"node expected but get a function"
"type combination error: <type> expected"
"arity error: <int> expected instead of <int>"
"operation not allowed in static expression: <operation>"
"array index '<int>' out of bounds '<int>..<int>"
"type error in array: <type> mixed with <type>"
"array of tuple not allowed"
"empty array"
"constant is declared as <type> but evaluated as <type>"
"bad constant value: tuple not allowed"
"field is declared as <type> but evluated as <type>"
"bad field value: tuple not allowed"
"bad static expression: <ident> is a variable"
"bad array size <int>"
"bad array size"

----------------------------------------------------------------------*)

(* une exception maison, faite pour etre catch�e *)
exception Failed

(*---------------------------------------------------------------------
Une erreur associ�e � un lexeme dans le fichier source
----------------------------------------------------------------------*)
exception Compile_error of Lexeme.t * string
exception Compile_error_opt of Lexeme.t option * string

(*---------------------------------------------------------------------
Une erreur plus generale
----------------------------------------------------------------------*)
exception Global_error of string

(*---------------------------------------------------------------------
Une erreur qui n'aurrait pas du arriver (fonction, message)
----------------------------------------------------------------------*)
exception Internal_error of string * string


(*---------------------------------------------------------------------
Formatage standard des lexemes dans les messages d'erreur
----------------------------------------------------------------------*)
val lexeme_details : Lexeme.t -> string
val lexeme_line_col : Lexeme.t -> string

(*---------------------------------------------------------------------
Formatage standard des erreurs de compil
----------------------------------------------------------------------*)
val compile_error_string : Lexeme.t -> string -> string

(*---------------------------------------------------------------------
Message d'erreur (associ� � un lexeme) sur stderr
----------------------------------------------------------------------*)
val print_compile_error :Lexeme.t -> string -> unit

(*---------------------------------------------------------------------
Warning (associ� � un lexeme) sur stderr
----------------------------------------------------------------------*)
val warning : Lexeme.t -> string -> unit

(*---------------------------------------------------------------------
Message d'erreur (sans lexeme) sur stderr
----------------------------------------------------------------------*)
val print_global_error : string -> unit

(*---------------------------------------------------------------------
Message d'erreur interne (avec fonction) sur stderr
----------------------------------------------------------------------*)
val print_internal_error : string -> string -> unit
