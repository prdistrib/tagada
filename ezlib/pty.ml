(*----------------------------------------------------------------------
	module : Pty
	date :
------------------------------------------------------------------------

----------------------------------------------------------------------*)

type t = {
   mutable _oc : out_channel;
   mutable _tab : int;
   mutable _tlevel : int;
   mutable _curpos : int;
   mutable _width : int;
}

let make (oc: out_channel) : t = {
   _oc = oc; _tab = 3; _tlevel = 0; _curpos = 0; _width = 100
}

let tab (x: t) = x._tlevel <- x._tlevel +1
let untab (x: t) = x._tlevel <- x._tlevel -1

(* internal !!! *)
let _pc x c = (
   output_char x._oc c;
   x._curpos <- x._curpos + 1
)

let cr (x:t) =
   output_char x._oc '\n';
   x._curpos <- 0;
   for _ = 1 to (x._tab * x._tlevel) do
      _pc x ' '
   done

let prints (x: t) (s: string) : unit = (
   let cs = String.to_seq s in
   let rec puts cs = match cs () with
   | Seq.Nil -> () 
   | Seq.Cons ('\n', _) -> cr x
   | Seq.Cons (' ', r) -> (
      if (x._curpos >= x._width) then cr x
      else _pc x ' '; puts r
   )
   | Seq.Cons (c, r) -> _pc x c; puts r
   in
   puts cs
)

let printf (x: t) fmt =
   Printf.kprintf (fun s -> prints x s) fmt
