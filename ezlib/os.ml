(*
   some quick functionality not (yet) offered by
   Sys, Unix, Filename etc.
*)

(* recursive rm *)
let rec rmrf path = match Sys.is_directory path with
  | true ->
    Sys.readdir path |>
    Array.iter (fun name -> rmrf (Filename.concat path name));
    Unix.rmdir path
  | false -> Sys.remove path

(* silently create an empty dir, remove it if it already exists *)
let reset_dir nm =
   let _ = try (rmrf nm) with _ -> () in
   Sys.mkdir nm 0o777
