(*----------------------------------------------------------------------
 *	module : Verbose
 *	date :
 * 2016/02: add "os" (largely useless for the moment
 *          since necessarily  os=stderr !)
 * 2016 replaced "level" and "flag" by "l" anf "f"
 * 2010
------------------------------------------------------------------------
	description :

	Affichage verbeux avec appel "printf-like" :

Verbose.put "format" args...
----------------------------------------------------------------------*)

(* $Id$ *)

(** Verbose printing manager

   Conditionnal [printf]-like, depending on global verbose-level and/or flag:
   - level is a int (0=silent, 1=verbose, 2=more verbose, etc.)
   - a [flag] is any string
   - Integration with command-line parsing: {!Ez.Cmd} provides a predefined
     [-dbg <pattern>] option, based on all flags declared in [Verbose]

*) 


(** {1 Levels management} *)

val on : unit -> unit
(** [on ()] sets level 1 *)

val off : unit -> unit
(** [off ()] sets level 0 *)

val set : int -> unit
(** [set i] sets level [i] *)

val os : unit -> out_channel

(** {1 Flags management} *)

(**
Typical usage: define a debug flag.

In module [Foo]:
   - add a GLOBAL declaration:
      [let dbg = Global.get_dbg_flag "foo"]
   - then in the code use:
      {ul
      {- [Verbose.printf ~f:dbg ...]}
      {- [Verbose.exe ~f:dbg ...]}}
   - in the 'main' program, sets the flag if needed:
      {ul
      {- [Verbose.set_flag Foo.dbg]}}

Integaration with command-line parsing:
   - {!Ez.Cmd} provides a default [-dbg] option to ease the (optional) setting of flags
*)

type flag
(** Abstract [flag] type *)

val get_flag : string -> flag
(** Create a [flag], DOES NOT SET IT *)

val set_flag : flag -> unit
(** Sets a created [flag] type *)

val flag_list : unit -> string list
(** Gets the list of created flags, so far *)

(* val level : unit -> int *)

(** {1 Conditional verbose} *)

(** Print string or exec command if EITHER:
   - specified [l](evel) (dflt=1) is greater than the set level
   - specified [f]lag is currently set
WARNING, formated string usage:
   - in the first version, evaluation of the formated string was lazzy,
   using GORE and UNSAFE feature
   - in this version, the evaluation occurs even when versose is not set,
   which may lead to efficiency problem
   - THUS: use [printf] for SIMPLE format (e.g., few %d, %s)
   - AND: use [exe] whenever the text to print is complex (e.g., rec structures)
*)

val printf : ?l:int -> ?f:flag -> ('a, unit, string, unit) format4 -> 'a
(** print  formated string if [l]evel or [f]lag condition is set *)

val print_string : ?l:int -> ?f:flag -> string -> unit 
(** print string if [l]evel or [f]lag condition is set *)

val exe : ?l:int -> ?f:flag -> (unit -> unit) -> unit
(** exec procedure if [l]evel or [f]lag condition is set *)

(** {1 Conditional verbose with 'why' prefix} *)

(** 'WHY' system:
   - when using [xprintf] or [xprint_string], text
      text is prefixed with a 'why' string
   - When called outside some 'exe' procedure,
     'why' is determined by the [l]evel and/or [f]lag:
     {ul
     {- [dbg] for f=dbg}
     {- [v] for l=1, [v2] for v=2 etc.}}
   - When called INSIDE an [exe] procedure, 'why' prefix
     is automatically inherited from the [exe] args.
   - If it is not sufficient, use [Verbose.why ()]
      INSIDE a 'exe' procedure to force the printing
      of the 'why' prefix. 
*)

val xprintf : ?l:int -> ?f:flag -> ('a, unit, string, unit) format4 -> 'a
(** print  formated string if [l]evel or [f]lag condition is set *)

val xprint_string : ?l:int -> ?f:flag -> string -> unit 
(** print string if [l]evel or [f]lag condition is set *)

val why : unit -> unit
(** force printing of the 'why' prefix, use in 'exe' *)

(*
N.B. within 'exe' context, printf/print_string inherits l/f of the context
*)
