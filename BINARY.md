# TAGADA BINARY
File: tagada-0.5-Linux-x86_64.zip
Compiled on: Ubuntu 22.04.5 LTS
Should probably run on other similar Linux-x86_64
Required dynamic libraries:
- linux-vdso.so.1
- libm.so.6
- libc.so.6
- /lib64/ld-linux-x86-64.so.2
