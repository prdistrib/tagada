(*
Dump dag as MIA-compliant yaml
The dag can/should be MIA compliant,
i.e. it provides attibutes:

TASK EXECUTION ABSTRACTION (aka task trace)
-------------------------------------------

- They  MUST be defined for each task
- Historically, they are stored outside the yaml,
  one file '<foo-trace>.dat' for each task, specified with:

T   traceName: <foo-trace>  

  The folder where to find the trace files is not part of
  the yaml, and should be passed as argument to the MIA tools.
   
  Attributes are:
T    procDemand: <int>
T    memSize:    <int>
T    memDemand:  <int>

REAL-TIME REQUIREMENTS
----------------------

- Global info, that can be used by map/sched algos

G    deadline: <int>

- Stangely there is no task deadline info ?

MAPPING 
-------

- Can be set by map/sched algos, for each task:

T  assignedTo: <int>
T  order:      <int>
T  release:    <int>

- And a global attribute, should normally be equal (or greter)
  that the greatest 'assignedTo

G  numOfCores: <int>

*)

module P = Printf
module U = Ez.Utils
module R = Ez.Rand
module H = Hashtbl
module V = Ez.Verbose
module T = MiaTask
module MU = MiaUtils

(* UGLY: tries to get the number hidden within a string *)
let isdigit c = (c >= '0' && c <= '9')
let get_hidden_int s =
   let b = Buffer.create 10 in
   String.iter (fun c -> if isdigit c then Buffer.add_char b c) s;
   int_of_string (Buffer.contents b) 
let compare_tname s1 s2 = try (
   let n1 = get_hidden_int s1 in 
   let n2 = get_hidden_int s2 in 
   compare n1 n2
) with _ -> compare s1 s2

(* PARTIALLY OBSOLETE
   SHOULD PROBABLY BE SET BY THE USER
   extra-info = memDemand
   expect 2 parameters (not necessarily used)
   memsize and wcet
*)

let dummy_gmd _ms _wc = (
   V.printf "WARNING. 'ToMia.gen_mem_demand' not provided, using dummy values\n";
   R.int_between 1 10
)

let gen_mem_demand : (int -> int -> int) ref = ref dummy_gmd

(* If map is given,
task's 'assignedTo' and  'order', and field 'numOfCores' are filled according to
*)
let so2s = U.string_of_option (fun x -> x)
let dump_yaml ?(noprofiles=false) ?(folder=".") (sysname: string) (d: Dag.t) : string = (
   (* Open yaml file *)
   let opath = Filename.concat folder (sysname^".yaml") in
   let os = open_out opath in

   (* Create/reset the folder for storing the trace files *)
   let tfolder = if noprofiles
      then None
      else Some (MU.reset_prof_folder ~folder sysname)
   in
   V.printf "# gen mia  file: %s\n# gen mia profs: %s\n" opath (so2s tfolder);

   let tab = ref 0 in
   let _outtab () = output_string os (String.make (2 * !tab) ' ') in
   let outl s = _outtab(); P.fprintf os "%s\n" s in
   let outfi f i = _outtab(); P.fprintf os "%s: %d\n" f i in
   let outfs f s = _outtab(); P.fprintf os "%s: %s\n" f s in
   (* ident of a task *)
   let t2s tid = Dag.get_task_name d tid in
   (* sort tasks accordiding to name *)
   let nbt = Dag.nb_tasks d in
   let tl = List.init nbt (fun x -> x) in
   let cmp_task i j =
      compare_tname (Dag.get_task_name d i) (Dag.get_task_name d j)
   in
   let stl = List.stable_sort cmp_task tl in
   outl "nodes:" ;
   incr tab;
   let dump_task i = (
      let zetask = Dag.get_task d i in
      outl ((t2s i)^":");
      incr tab;
         (* SUCCS *)
         let _ = match Dag.get_succs d i with
         | [] -> outl "accessTo: []";
         | sl -> ( 
            outl "accessTo:";
            List.iter (
               fun s -> outl (
                  P.sprintf "- [%s, %d]"
                     (t2s s) 
                     (Dag.get_ewidth d (i,s))
               )
            ) sl
         ) in
         (* PREDS *)
         let pl = U.string_of_list ~sep:", " t2s (Dag.get_preds d i) in
         outl ("deps: "^pl);
         (* TRACE NAME *)
         let tn = P.sprintf "%s_%s-trace" sysname (t2s i) in
         outfs "traceName" tn;
         (* INFOS TO PUT IN yaml ANDOR  trace.dat file *)
         (* Task attributes ...*)
         let wc = T.wcet zetask in
         let msz = T.memsize zetask in
         let md = T.memdemand zetask in
         let _ = match tfolder with
            | None -> ()
            | Some tfolder -> (
               let tnfile = Filename.concat tfolder (tn^".dat") in
               let tnos = open_out tnfile in
               (* ... to be put in corresponding dat file *)
               P.fprintf tnos "procDemand %d\n" wc;
               P.fprintf tnos "memSize %d\n" msz;
               P.fprintf tnos "memDemand %d\n" md;
               close_out tnos;
            )
         in
         (* TODO obsolete, keep them commented in the yaml *)
         outfi "procDemand" wc;
         outfi "memSize" msz;
         outfi "memDemand" md ;
         (* MISC UNUSED *)
         outfi "assignedTo" (T.core zetask);
         outfi "order" (T.order zetask);
         outfi "release" (T.release zetask);
      decr tab
   ) in
   List.iter dump_task stl;
   (* OTHERS DAG ATTRIBUTES, blind dump ... *)
   decr tab;
   (Dag.att_seq d) |> Seq.iter (
      fun (k,v) -> match v with
         | `Int i    -> outfi k i 
         | `String s -> outfs k s
   );
   close_out os;
   opath
)
