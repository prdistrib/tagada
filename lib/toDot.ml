(* dump dag as MIA-compliant yaml *)

module P = Printf
module U = Ez.Utils
module R = Ez.Rand
module V = Ez.Verbose

module T = MiaTask

(*
   node label described by a string:
   - n = name
   - w = wcet
   - m = memory size
*)
let _node_label = ref "n"
let set_node_label s = _node_label := s

let node_label (zedag: Dag.t) (tid: int) =  
   let ld = List.init (String.length !_node_label) (String.get !_node_label) in
   let tsk = Dag.get_task zedag tid in
   let d2s = function
      | 'n' -> P.sprintf "t%02d" tid
      | 'w' -> string_of_int (T.wcet tsk)
      | 'm' -> string_of_int (T.memsize tsk)
      | x -> (
         let msg = P.sprintf "ToDot.node_label: unknown flag \'%c\'" x in
         raise (Invalid_argument msg)
      )
   in 
   let ls = List.map d2s ld in
   U.string_of_string_list ~obr:"" ~cbr:"" ~sep:"\\n" ls

(* node label *)
let dump ?(folder=".") (sysname: string) (zedag: Dag.t) : string = (
   let opath = Filename.concat folder (sysname^".dot") in
   V.printf "# gen dot with labels '%s': %s\n" !_node_label opath;
   let os = open_out opath in
   (* let os = stdout in *)
   let t2s tid = P.sprintf "t%02d" tid in
   let ptask i = 
      let t = t2s i in
      P.fprintf os "%s [label=\"%s\"];\n"  t (node_label zedag i)
   in
   let ptrans src dest =
      let w = Dag.get_ewidth zedag (src,dest) in
      P.fprintf os "%s -> %s [label=\"%d\"];\n" (t2s src) (t2s dest) w
   in
   (* let tab = ref 0 in *)
   (* let _outtab () = output_string os (String.make (2 * !tab) ' ') in *)
   (* let outl s = _outtab(); P.fprintf os "%s\n" s in *)
   (* ident of a task *)
   P.fprintf os "digraph %s {\n" sysname;
   (* incr tab; *)
   let nbt = Dag.nb_tasks zedag in
   for i = 0 to (nbt-1) do
      (* incr tab; *)
      ptask i;
      (* SUCCS *)
      List.iter (ptrans i) (Dag.get_succs zedag i);
   done;
   (* UNUSED ? *)
   (* decr tab; *)
   P.fprintf os "}\n";
   close_out os;
   opath
)

