(* WARNING: imperative struct
This dag type IS dedicated to MIA format, with some predifined attributes:
   deadline     us(ed/able) by map/sched tools (?)
   numOfCores   used by  map/sched tools
We add a new one to ease info search and generation:
   sysName

*)

module P = Printf
module H = Hashtbl
module R = Ez.Rand
module U = Ez.Utils
module V = Ez.Verbose
module T = MiaTask

(* module TaskSet = U.XSet (Task) *)
(* module I2Task = U.XMap (U.XInt) (Task) *)

(*
   - task identified by id: int
   - use int to int sets Hash struct to ease removal of edge
   - ewidths are stored in a dedicated (int * int) table
     (just the weight for the time being)
*)

module ISet = U.IntSet
type tid = int
type tid_set = ISet.t

(* edge width =  amount of data transmited *)
type ewidth = int
(* let ewidth_dflt = 0  *)

(* Management of attributes,
   they must be specified at creation
*)

type attval =
[  `Int of int
|  `String of string
]
type att = string * attval

type t = {
   (* tasks: (int, Task.t) H.t; *)
   tasks: (int, T.t) H.t;
   taskids: (string, int) H.t;
   (* use find_all to get the whole lists *)
   preds: (int, ISet.t) H.t;
   (* amount of data transmited is stored in succs *)
   succs: (int, ISet.t) H.t;
   ewidths: (int * int, ewidth) H.t;
   atts: (string, attval) H.t;
}

let set_att d k v =
   try let _ = H.find d.atts k in H.replace d.atts k v 
   with Not_found -> Global.exit ("Dag.set_att, attribute '"^k^"' not registred") 

let get_att d k = 
   try H.find d.atts k
   with Not_found -> Global.exit ("Dag.get_att, attribute '"^k^"' not registred") 

let att_seq d = H.to_seq d.atts

(* hard copy *)
let copy d = {
   tasks = H.copy d.tasks;
   taskids = H.copy d.taskids;
   preds = H.copy d.preds;
   succs = H.copy d.succs;
   ewidths = H.copy d.ewidths;
   atts = H.copy d.atts;
}

(* internal *)
let _add_link ht x y =
   let p = try H.find ht x with Not_found -> ISet.empty in
   H.replace ht x (ISet.add y p)
let _rm_link ht x y =
   let p = try H.find ht x with Not_found -> ISet.empty in
   H.replace ht x (ISet.remove y p)
let get_task d i = H.find d.tasks i
let get_task_name d i = T.to_string (H.find d.tasks i)

let get_succs d i = try ISet.elements (H.find d.succs i) with Not_found -> []
let get_preds d i = try ISet.elements (H.find d.preds i) with Not_found -> []
(* let get_ewidth d (i,j) = try H.find d.ewidths (i,j) with Not_found -> ewidth_dflt  *)
let get_ewidth d (i,j) = H.find d.ewidths (i,j)
(* size does not have to be precise, just to init hash tables *)
let create size attlist = {
      tasks = H.create size;
      taskids = H.create size;
      preds = H.create size;
      succs = H.create size;
      ewidths = H.create size;
      atts = H.of_seq (List.to_seq attlist); 
   }
let add_edge_to (dag:t) (d: tid) (width:int) (s: tid): unit = (
   _add_link dag.succs s d;
   _add_link dag.preds d s;
   H.replace dag.ewidths (s,d) width
)
let add_edge_from (dag:t) (s: tid) (width:int) (d: tid): unit = (
   _add_link dag.succs s d;
   _add_link dag.preds d s;
   H.replace dag.ewidths (s,d) width
)
(* OBSOLETE PARTIALLY
random choice of edge 'width' (amount of data transmited)
may depend on memsize and wcet OF SOURCE 
*)
let gew_dummy _ _ = (
   V.printf "WARNING. 'Dag.gen_edge_width not provided, using dummy values";
   R.int_between 1 10
)
let gen_edge_width : (T.t -> T.t -> int) ref = ref gew_dummy

let create_edge_to (dag:t) (dest: T.t) ?(width=(-1)) (src: T.t): unit = (
   let (s,d) = (T.id src, T.id dest) in
   (* au pif: amount of data between 1 and 25% of mem size *)
   let width = if width > 0 then width
       else !gen_edge_width src dest
   in
   add_edge_to dag d width s
)
let remove_edge dag (s,d) = (
   _rm_link dag.succs s d;
   _rm_link dag.preds d s;
   H.remove dag.ewidths (s,d)
)

let add_task (d:t) (t: T.t): unit = (
   H.add d.tasks (T.id t) t;
   H.add d.taskids (T.to_string t) (T.id t)
)
let nb_tasks (d:t) = H.length d.tasks
let taskid (d:t) (n:string) : int = H.find d.taskids n

(* dump/debug *)
let dump (os: out_channel) (zedag: t) = (
   (* let os = stdout in *)
   let ptask i = 
      let tsk = get_task zedag i in
      P.fprintf os "%d %s succs={" i (T.dbg_string tsk);
      List.iter (
         fun j -> P.fprintf os "(%d, %d) " i (get_ewidth zedag (i,j))
      ) (get_succs zedag i);
      P.fprintf os "}\n"
   in
   let nbt = nb_tasks zedag in
   for i = 0 to (nbt-1) do
      (* incr tab; *)
      ptask i;
   done
)

(* 1st version, translate as it, no pruning no optim *)
let of_parseq_raw (x: ParSeq.t) (al: att list): t = (
   let zedag = create (T.card ()) al in
   let rec ps2dag (preds: T.t list) (x: ParSeq.t) : T.t list =
      match x with
      | Task t -> (
         add_task zedag t;
         List.iter (create_edge_to zedag t) preds ; [t]
      )
      | Seq [x] -> ps2dag preds x
      | Seq (s::sl) -> let preds' = ps2dag preds s in ps2dag preds' (Seq sl)
      | Par ppl -> List.flatten (List.map (ps2dag preds) ppl) 
      | _ -> assert false
   in
   let _ = ps2dag [] x in
   (* remove some edge to make it more 'realistic' ? *) 
   (* list of (n, preds) s.t. 
      - preds is strictly greater than 1
      - p in preds has more than 1 succ
   *)
   zedag
)

(* 2nd version, with pruning policy to reduce the
   'crossbar' effect of random parseq
Reduce edge policy:
- first pass (construction):
   * a task 'x' whose pot-preds is not empty:
     - MUST have at least one actual pred (chosed randomly)
     - other pot-preds become actual-pred with proba (100-prune) %
     - for non-chosen pot-pred 'p' we recall that 'x' is a pot-succ
- second pass:
   * for all task with SOME pot-succs AND NO actual succ
     we choserandomly one actual succ 
N.B.:
- (hopefully) those 2 commands are equivalent:
   'of_parseq ~prune=0 x'
   'of_parseq_basic x'
   while the 'prune' version is probably more time-consuming
- if the parSeq exp if in 'fork/join' normal form, pruning
  has (hopefully) no influence, since all task has
  either a unique pred or a unique succ 
*)
let of_parseq ?(prune=0) (x: ParSeq.t) (al: att list): t = (
   let zedag = create (T.card ()) al in
   let pot_succs = Hashtbl.create 100 in

   (* 1st pass: add at least one pred to t, perhaps some more randomly *)
   let rec ps2dag (preds: T.t list) (x: ParSeq.t) : T.t list =
      match x with
      | Task t -> (
         add_task zedag t;
         let nbp = List.length preds in
         let _ = if (nbp > 0) then (
            let mandatory_pred = List.nth preds (R.int_between 0 (nbp-1)) in
            create_edge_to zedag t mandatory_pred;
            let random_add_pred p =
               if (p <> mandatory_pred) && (R.percent prune) then
                  _add_link pot_succs (T.id p) (T.id t)
               else 
                  create_edge_to zedag t p
            in
            List.iter random_add_pred preds ;
         ) in
         [t]
      )
      | Seq [x] -> ps2dag preds x
      | Seq (s::sl) -> let preds' = ps2dag preds s in ps2dag preds' (Seq sl)
      | Par ppl -> List.flatten (List.map (ps2dag preds) ppl) 
      | _ -> assert false
   in
   let _ = ps2dag [] x in
   (* 2nd pass: check for 'sink' task, i.e. with pot_succs and succs empty *)
   let check_sink (x:int) (pxs: ISet.t) = (
      match get_succs zedag x with
      | [] -> (
         let pxs = ISet.elements pxs in
         let nb_pxs = List.length pxs in
         let axs = List.nth pxs (R.int_between 0 (nb_pxs-1)) in
         create_edge_to zedag (get_task zedag axs) (get_task zedag x)
      )
      | _ -> () 
   ) in
   Hashtbl.iter check_sink pot_succs;
   zedag
)
