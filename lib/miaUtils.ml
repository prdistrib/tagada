(* Shared by ToMia, OfMia *)

(* *)

open Printf
module FN=Filename

(* to make code more clear *)
let (@@@) = FN.concat;;

(* - returns the system name of the profile folder
   - does not test if it exists
*)
let get_prof_folder ?(folder=".") sysname : string =
   folder @@@ (sysname^"_profiles") @@@ "baseArchitecture"

(* - if the prof folder already exist, cleanup ir
   - if not create it
*)
let reset_prof_folder ?(folder=".") sysname : string = (
   let toppf = (
      let res = folder @@@ (sysname^"_profiles") in
      if Sys.file_exists res then (
         if Sys.is_directory res then
            res
         else 
            Global.exit (sprintf "'%s' exists and is not a folder" res) 
      ) else (
         Sys.mkdir res 0o777;
         res
      )
   ) in
   let pf = toppf @@@ "baseArchitecture" in
   Ez.Os.reset_dir pf;
   pf
)

(* Default MIA attributes with default values
sysName is intruced by tagada lib (not in original MIA's yaml)
*)

let dag_atts = [
   ("sysName", `String "");
   ("deadline", `Int 10000000000000);
   ("numOfCores", `Int 8)
]

