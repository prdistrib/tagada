

(*
Priority queue
- this is an IMPERATIVE/MUTABLE structure
- pick order is deterministic, when 2 elts have the same
  priority, the FIFO principle is applyed

WARNING: no balencing
- complexity is between log n and n
*)

type 'a t

val create: ?cmp:('a -> 'a -> int) -> unit -> 'a t

val push: 'a t -> 'a -> unit

exception Queue_is_empty
 
val pop : 'a t -> 'a

val is_empty: 'a t -> bool

