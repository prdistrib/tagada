(*
Extra attributes can be added
e.g. "memDemand"
*)

type t

val id: t -> int

val to_string: t -> string
val dbg_string: t -> string

val create : ?nme:string -> unit  -> t

val card: unit -> int

val compare: t -> t -> int

(* attributes management
returns access functions (get, set)
*)
val register_att: string -> int -> (t -> int) * (t -> int -> unit)
val att_list: t -> (string * int) list

(* OBSOLETE
random memsize utility
val set_max_memsize: int -> unit
val low_ms: unit -> int
val normal_ms: unit -> int
val high_ms: unit -> int
*)
