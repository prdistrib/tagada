(*
Strings for specifying values in command line:
- rand natural integer around a given value 
- rand natural integer within an interval
- if percent is specified, check that values are < 100
*)
module P=Printf
module S=Str

(* module RandSpec = struct *)
   type t =
   | Around of int
   | Range of int * int

   let to_string = function
   | Around x -> P.sprintf "Around %d" x
   | Range (x,y) -> P.sprintf "Range (%d,%d)" x y

   let make_around i = (assert (i >= 0); Around i)
   let make_range i j = (assert (i <= j); Range (i,j))

   let of_string ?(percent=false) s = 
      (* Some stuff to ease regexp *)
      let integer = "[0-9]+" in
      let group x = "\\("^x^"\\)" in
      let around = "~"^(group integer)^"$" in
      let range = "\\["^(group integer)^","^(group integer)^"\\]$" in
      let domatch r s = S.string_match (S.regexp r) s 0  in
      let mgrp = S.matched_group in
      let ios = fun s -> 
         let v = int_of_string s in
         assert ((not percent) || ((v >= 0) && (v <= 100)));
         v
      in
      try (
         if domatch around s then (
            make_around (ios @@ mgrp 1 s)
         ) else if domatch range s then (
            make_range (ios @@ mgrp 1 s) (ios @@ mgrp 2 s)
         ) else  assert false
         ) with _ -> (
            raise (Invalid_argument ("bad rand spec '"^s^"'"))
         )
(* end *)
