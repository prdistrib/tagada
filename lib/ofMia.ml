(* read a MIA-compliant yaml *)
(*
The dag can/should be MIA compliant,
i.e. it provides attibutes:

TASK EXECUTION ABSTRACTION (aka task trace)
-------------------------------------------
- They  MUST be defined for each task
- They are stored outside the yaml,
  one file '<foo-trace>.dat' for each task, specified with:
T   traceName: <foo-trace>  

  The folder where to find the trace files is not part of
  the yaml, and should be passed as argument to the MIA tools.
  However, there is a 'usual' name for the folder, see MiaUtils
  Attributes are:
T    procDemand: <int>
T    memSize:    <int>
T    memDemand:  <int>

REAL-TIME REQUIREMENTS
----------------------
- Global info, that can be used by map/sched algos
G    deadline: <int>
- Stangely there is no task deadline info ?

MAPPING 
-------
- Can be set by map/sched algos, for each task:
T  assignedTo: <int>
T  order:      <int>
T  release:    <int>

- And a global attribute, should normally be equal (or greter)
  that the greatest 'assignedTo
G  numOfCores: <int>

*)


(* type value =
  [ `Null
  | `Bool of bool
  | `Float of float
  | `String of string
  | `A of value list
  | `O of (string * value) list
]
*)

module T = MiaTask
module P = Printf
module V = Ez.Verbose
module MU = MiaUtils

let dbg = V.get_flag "ofMia"

let get_dict_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`O l) -> l
   | _ -> Global.exit (P.sprintf "no dictionnary '%s' field" key) 

let get_list_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`A l) -> l
   | _ -> Global.exit (P.sprintf "no list '%s' field" key) 

let get_int_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`Float f) -> (int_of_float f)
   | _ -> Global.exit (P.sprintf "no int '%s' field" key) 

let get_string_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`String s) -> (s)
   | _ -> Global.exit (P.sprintf "no string '%s' field" key) 

(* same but simply raise Not_found *)
let try_get_int_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`Float f) -> (int_of_float f)
   | _ -> raise Not_found

let try_get_string_field key y =
   match Yaml.Util.find_exn key y with
   | Some (`String s) -> (s)
   | _ -> raise Not_found

(* more specific *)
let yaml2edge y =
   match y with
   | `A [`String s; `Float f] -> (s, int_of_float f)
   | _ -> Global.exit (P.sprintf "bad formated 'accessTo'") 

(*
   if 'profiles' folder is specified, extra-functionnal info
   of task 't' are searched in
   profiles/{t.traceName}.dat
*)
let wsre = Str.regexp "[ \n\r\x0c\t]+"
let read_dat f = (
   let ic = try open_in f with
      _ -> Global.exit (P.sprintf "Can't open '%s'" f)
   in
   let rec read_lines () =
      try (
         let l = input_line ic in
         try (
            let x = match Str.split wsre l with
            | [k; v] -> (k, int_of_string v)
            | _ -> raise Not_found
            in
            x::(read_lines ())
         ) with _ -> Global.exit (P.sprintf "unexpected profile content '%s'" l)
      ) with End_of_file -> []
   in
   read_lines ()
)

let f (profiles: string) (yv : Yaml.value) : Dag.t = (
   (* check for implicit profiles folder
      requires sysName arg works only for yaml/tagada
   *)
   let profiles = match profiles with
   |  "" -> (
      try 
         let sysname = try_get_string_field "sysName" yv in
         let pf = MU.get_prof_folder sysname in
         assert ((Sys.file_exists pf) && (Sys.is_directory pf));
         pf
      with _ -> Global.exit ("can't guess profiles folder, please specify it")
   )
   | p -> p
   in

   let nodes = get_dict_field "nodes" yv in
   (* V.xprintf ~f:dbg "nb nodes = %d\n" (List.length nodes); *)
   V.exe  ~f:dbg (fun () ->
      V.xprintf  "nb nodes = %d\n" (List.length nodes)
   );

   (* dag init size do not need to be exact (but it is actually) *)
   let zedag = Dag.create (List.length nodes) MU.dag_atts in 
   (* try to recover all expected atts *)
   MU.dag_atts |> List.iter (
      fun (k,v) -> match v with
      |  `Int _ -> (
         try  Dag.set_att zedag k (`Int (try_get_int_field k yv))
         with Not_found -> ()
      )
      |  `String _ -> (
         try Dag.set_att zedag k (`String (try_get_string_field k yv))
         with Not_found -> ()
      )
   );
   (* first pass create tasks *)
   let create_task = function (nme, content) ->
      (* where to get (profile) atts *)
      let datfile = (get_string_field "traceName" content)^".dat" in
      let datfile = Filename.concat profiles datfile in
      let datlist = read_dat datfile in
      let get_att key = try List.assoc key datlist
         with _ -> Global.exit (P.sprintf "missing %s in '%s'" key datfile)
      in
      let ms = get_att "memSize" in
      let wcet = get_att "procDemand" in
      V.xprintf ~f:dbg "task: %s wcet = %d mem size = %d\n" nme wcet ms;
      let task = Task.create ~nme:nme () in
      T.set_wcet task wcet;
      T.set_memsize task ms;
      T.set_memdemand task (get_att "memDemand");
      T.set_core task (get_int_field "assignedTo" content);
      T.set_order task (get_int_field "order" content);
      T.set_release task (get_int_field "release" content);
      Dag.add_task zedag task 
   in
   List.iter create_task nodes;
   (* second pass create edges *)
   let create_edges = function (nme, content) ->
      let src = Dag.get_task zedag (Dag.taskid zedag nme)in
      let yel = get_list_field "accessTo" content in
      let add_edge ye =
         let (dn, a) = yaml2edge ye in
         let dest = Dag.get_task zedag  (Dag.taskid zedag dn)in
         V.xprintf ~f:dbg "trans: %d -%d-> %d\n" (T.id src) a (T.id dest);
         Dag.create_edge_to zedag dest ~width:a src
      in
      List.iter add_edge yel
   in
   List.iter create_edges nodes;
   zedag
)
