

(*
Priority queue
- this is an IMPERATIVE/MUTABLE structure
- pick order is deterministic, when 2 elts have the same
  priority, the FIFO principle is applyed

WARNING: no balencing for the time being !
- complexity is between log n and n
*)

type 'a bst =
   | Empty
   | Node of int * 'a * 'a bst * 'a bst
let depth = function
   | Empty -> 0
   | Node (d,_,_,_) -> d

type 'a t = {
   cmp: 'a -> 'a -> int;
   mutable tree: 'a bst
}

let create ?(cmp=compare) () = { cmp; tree=Empty }

let push (q: 'at) (x:'a) : unit  = (
   let rec insert x bt = match bt with
      | Empty -> Node(1, x, Empty, Empty)
      | Node (_, value, infs, supeqs) -> 
         if (q.cmp x value) < 0 then 
            (* stricly inf, insert in infs *)
            let infs' = insert x infs in
            Node ( 1 + max (depth infs') (depth supeqs),
               value, infs', supeqs)
         else
            (* eq or sup, insert in supeq
               side effect: guaranties FIFO when equal *)
            let supeqs' = insert x supeqs in
            Node ( 1 + max (depth infs) (depth supeqs'),
               value, infs, supeqs')
   in
   q.tree <- insert x q.tree
)
exception Queue_is_empty
 
let pop (q: 'at) : 'a = (
   let rec getmin = function
      | Empty -> raise Queue_is_empty
      | Node (_,x,Empty,supeqs) -> (x, supeqs)
      | Node (_,value,infs,supeqs) -> (
         let (x, infs') = getmin infs in 
         let node' = Node (1 + max (depth infs') (depth supeqs), value, infs', supeqs) in
         (x, node')
      )
   in
   let (x, tree') = getmin q.tree in
   q.tree <- tree';
   x
)
let is_empty q = (depth q.tree) = 0

