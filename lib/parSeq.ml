
module R = Ez.Rand
module P = Printf
module V = Ez.Verbose
let dbg = V.get_flag "ParSeq"

(* Dedicated to MiaTask 
TODO move away MIA stuff ?
*)
module T = MiaTask

type t =
| Par of t list
| Seq of t list
| Task of T.t

(* dump *)
let rec dump os x = match x with
   | Seq l ->
      P.fprintf os "(";
      List.iteri (fun i y -> (if i > 0 then P.fprintf os " ; "); dump os y) l;
      P.fprintf os ")";
   | Par l ->
      P.fprintf os "(";
      List.iteri (fun i y -> (if i > 0 then P.fprintf os " | "); dump os y) l;
      P.fprintf os ")";
   | Task k -> output_string os (T.to_string k)

(* TODO intermediate solution, package globaly parameteriwed by TWO task creators :
   gen_normal_task wcet : T.t
   gen_extra_task () : T.t
*)
let gnt_dummy (_wcet: int) : T.t =
   Global.exit ("ParSeq.gen_normal_task not set")
let gxt_dummy () : T.t =
   Global.exit ("ParSeq.gen_extra_task not set")

let gen_normal_task: (int -> T.t) ref = ref gnt_dummy
let gen_extra_task: (unit -> T.t) ref = ref gxt_dummy


(* random ast *)
let _rand pproba wcet sz = 
   let rec _recrand wcet sz = (
      (* if (sz = 1) then Task (T.create wcet (!gen_mem_size())) *)
      if (sz = 1) then Task (!gen_normal_task wcet)
      else (
         (* let cut = 1 + (Random.int (sz-1)) in *)
         let cut = R.int_between 1 (sz-1) in
         if R.percent pproba then
            (* par: each wcet is ~ wcet *)
            let w1 = R.int_around wcet in
            let ps1 = _recrand w1 cut in
            let w2 = R.int_around wcet in
            let ps2 = _recrand w2 (sz-cut) in
            Par [ ps1; ps2 ]
         else
            (* seq: wcet is split ~ linear to sz *)
            (* let w1 = iof ((foi wcet) *. (foi cut) /. (foi sz)) in *)
            let w1 = (wcet * cut) / sz in
            let w2 = wcet - w1 in
            let w1 = R.int_around w1 in
            let w2 = R.int_around w2 in
            let ps1 = _recrand w1 cut in
            let ps2 = _recrand w2 (sz-cut) in
            Seq [ ps1; ps2 ]
      )
   )
   in _recrand wcet sz 
   
(* Basic normalization *)
let rec _fpar (acc: t list) (x: t) = match x with
   | Par l -> acc @ (_norm_par l)
   | Seq l -> acc @ [Seq (_norm_seq l)]
   | Task _ -> acc @ [x]
and _fseq (acc: t list) (x: t) = match x with
   | Seq l -> acc @ (_norm_seq l)
   | Par l -> acc @ [Par (_norm_par l)]
   | Task _ -> acc @ [x]
and _norm_par (l: t list) : t list = List.fold_left _fpar [] l 
and _norm_seq (l: t list): t list = List.fold_left _fseq [] l 
and normalize (x: t): t = (
   match x with
      | Seq l -> Seq (_norm_seq l)
      | Par l -> Par (_norm_par l)
      | Task _ -> x
)

(* Basic normalized random ast *)
let rand_norm ?(pproba=50) wcet sz = let res = (
   (* let source = Task (T.create (R.int_around 100) (!gen_low_mem_size())) in *)
   let source = Task (!gen_extra_task ()) in
   match sz with
   | 1 -> source
   | 2 -> (
      (* let sink = Task (T.create (R.int_around 100) (!gen_low_mem_size())) in *)
      let sink = Task (!gen_extra_task ()) in
      Seq [source; sink ]
   )
   | _ -> (
      let core = _rand pproba wcet (sz-2) in 
      (* let sink = Task (T.create (R.int_around 100) (!gen_low_mem_size())) in *)
      let sink = Task (!gen_extra_task ()) in
      let res = Seq [ source; core; sink ] in
      normalize res 
   )
) in (
   V.exe ~f:dbg ( fun () ->
      V.printf "ParSeq.rand_norn:\n# ";
      dump stderr res; P.fprintf stderr "\n"
   ); res
)

(* Fork-join normalized random ast *)
let rand_fj ?(pproba=50) wcet sz = let res = (
   (* let new_fj_task () = Task (T.create (R.int_around 100) (!gen_low_mem_size())) in *)
   let new_fj_task () = Task (!gen_extra_task ()) in
   let rec do_seq_args fork_needed = function
   | (Par sl)::pl -> (
      if fork_needed then
         let ft = new_fj_task() in
         let p' = Par (do_par_args sl) in
         ft::p'::(do_seq_args true pl)
      else 
         let p' = Par (do_par_args sl) in
         p'::(do_seq_args true pl)
   ) 
   | ((Task _) as tt)::pl ->
      tt::(do_seq_args false pl)
   | [] when fork_needed -> [new_fj_task ()]
   | [] -> []
   | _ -> assert false
   and do_par_args (sl: t list): t list = List.map (
      function
         | (Seq pl) -> Seq (do_seq_args true pl)
         | (Task _) as tt -> tt
         | _ -> assert false
      ) sl
   in
   (* gen a normalized core *)
   let core = _rand pproba wcet sz in 
   V.exe ~f:dbg ( fun () ->
      V.xprintf ~f:dbg "(rand_fj) c0 = ";
      dump (V.os ()) core ;
      V.printf ~f:dbg "\n"
   );
   let core = normalize core in
   V.exe ~f:dbg ( fun () ->
      V.xprintf ~f:dbg "(rand_fj) c1 = ";
      dump (V.os ()) core ;
      V.printf ~f:dbg "\n"
   );
   match core with 
   | Task _ -> core
   | Seq pl -> Seq (do_seq_args true pl)
   | Par sl -> (
      let top = new_fj_task () in
      let bot = new_fj_task () in
      Seq (top::(do_par_args sl)@[bot]) 
   )
) in (
   V.exe ~f:dbg ( fun () ->
      V.printf "ParSeq.rand_fj:\n# ";
      dump stderr res; P.fprintf stderr "\n"
   ); res
)
