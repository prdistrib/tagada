
module P = Printf
module H = Hashtbl
(* UTILS *)
module U = Ez.Utils
module V = Ez.Verbose
module Q = Queue
(* open Tagada *)

module T = MiaTask

let dbg = V.get_flag "map"

(* Structures for building a mapping tasks to cores *)

module Vcore = struct
   type t = {
      _index: int;
      _memsize: int;
      mutable _start_date: int;
      mutable _end_date: int;
      mutable _free_mem: int;
      _tasks: T.t Q.t;
   }
   let empty ix memsz = {
      _index = ix;
      _memsize = memsz;
      _start_date = 0;
      _end_date = 0;
      _free_mem = memsz;
      _tasks = Q.create ();
   }
   let dump os x = (
      let mu = x._memsize - x._free_mem in
      P.fprintf os " mem: %4d , time: %5d - %5d [" mu x._start_date x._end_date;
      let _ = Q.fold (fun p t -> P.fprintf os "%s%s" p (T.to_string t); "; ") "" x._tasks in
      P.fprintf os "]\n"
   )
end;;

(* number of cores is dynamic *)
type t = {
   memsz : int;
   maxc : int;
   mutable nbc : int;
   vcores : Vcore.t array;
}

let iter_cores (f : int -> T.t Seq.t -> unit) (it:t) = (
   Array.iteri (fun i vc -> f i (Q.to_seq vc.Vcore._tasks)) it.vcores
)
let nb_cores (it:t) = it.nbc

(* create an EMPTY array of maxc vcores
but only those < nbc are actually used *)
let create ?(useallcores=false) maxc msz = {
   memsz = msz;
   maxc = maxc;
   (* nbc is the number of used cores *)
   nbc = if useallcores then maxc else 0;
   vcores = Array.init maxc (fun i -> Vcore.empty i msz);
}

(* CREATE A MAPPING/ALLOCATION:
User can:
   - create a new (empty) map with a maxmem for each core and a max number of cores
   - allocate a new core, fails if max number of cores is reached
   - add (schedule) a task at the end of an already allocated core
      * fails if not enough memory on this core
      * the task may starts strictly after the current 'end' of the core (opti. 'startd') 
   - 'fold-i' any function on the already allocated cores, to, e.g. select
      a suitable core for schedulling some task
   - 'find_first' core satisfying a predicate
   - 'find_least' core according to a 'compare' function (the lower the better)
*)

(* schedule tsak tsk to (existing) core cx *)
let add_task (zemap:t) (cx: int) ?(startd=0) (tsk: T.t): unit = (
   assert ((cx >= 0) && (cx < zemap.nbc));
   let vc = zemap.vcores.(cx) in
   let real_start = max startd vc._end_date in
   let new_end = real_start + (T.wcet tsk) in
   let new_free = vc._free_mem - (T.memsize tsk) in 
   assert (new_free >= 0);
   vc._end_date <- new_end;
   vc._free_mem <- new_free;
   if Q.is_empty vc._tasks then vc._start_date <- startd; 
   Q.push tsk vc._tasks
)

(* add a new 'empty' vcore, fails if maxcore is reached *)
let new_core (zemap:t) = (
   let res = zemap.nbc in
   zemap.nbc <- zemap.nbc+1;
   assert (zemap.nbc < zemap.maxc);
   res
)
(* iterate on allocated cores *)
let vcores_foldi (zemap: t) (f: 'a -> int -> Vcore.t -> 'a) (accin: 'a): 'a = (
   let res = ref accin in
   for i = 1 to zemap.nbc do
      res := f !res (i-1) zemap.vcores.(i-1)
   done;
   !res
)

let find_first_core (zemap: t) (f: Vcore.t -> bool): int option = (
   let rec recfind n = 
      if n >= zemap.nbc then None
      else if f zemap.vcores.(n) then Some n
      else recfind (n+1)
   in
   recfind 0
)

(* Seach among the existing cores the 'least' one according to the comparizon 'cmp'
   Only consider the cores that are 'suitable' 
*)
let find_least_core (zemap: t) ?(suitable=fun _ -> true) (cmp: Vcore.t -> Vcore.t -> int): int option = (
   let rec recfind n res = 
      if n >= zemap.nbc then res
      else (
         let cn = zemap.vcores.(n) in
         let res = if suitable cn then (
            match res with
            | Some k ->
               let ck = zemap.vcores.(k) in
               if (cmp cn ck) < 0 then Some n else res
            | None -> Some n
         ) else res
         in 
         recfind (n+1) res
      )
   in
   recfind 0 None
)

let dump os zemap = (
   for i = 0 to zemap.nbc-1 do
      P.fprintf os "core %2d: " i ;
      Vcore.dump os zemap.vcores.(i)
   done
)

(******************** UTILS ******************************)
let get_wcet d tid = T.wcet (Dag.get_task d tid)


(* UTILS: 
   - "simulate" an idealized parralel ciomputation based on estimated wcets 
   - takes "ideal" begin and end dates of each task (see compute_dates)
*)
type evt = TBegin of int | TEnd of int
let string_of_evt i2s = function
   | TBegin i -> P.sprintf "(TBegin %s)" (i2s i)
   | TEnd i -> P.sprintf "(TEnd %s)" (i2s i)

let virtual_sched zedag bds eds = (
   (* for pty *)
   let i2s i = Dag.get_task_name zedag i in

   let vevts = H.create 100 in
   let add_begin i v = U.add_hash_list vevts v (TBegin i) in
   let add_end i v = U.add_hash_list vevts v (TEnd i) in
   Array.iteri add_begin bds; 
   Array.iteri add_end eds; 
   let d2evs = U.sorted_hash_content vevts in
   List.iter (fun (i,el) ->
      let el = List.rev el in
      P.printf "%4d: %s\n" i (U.string_of_list (string_of_evt i2s) el)
   ) d2evs;
   let apply_evts (a: U.IntSet.t) (evs: evt list): U.IntSet.t = 
      List.fold_left (fun a ev -> match ev with
           TBegin i -> U.IntSet.add i a
         | TEnd i -> U.IntSet.remove i a
      ) a evs
   in
   let (_, d2run) = List.fold_left_map (
      fun a (d, evs) ->
         let a' = apply_evts a evs in
         (a', (d,a'))
   ) U.IntSet.empty d2evs in
   let d2run: (int*int list) list =
      List.map (fun (d,ts) -> (d, U.IntSet.elements ts)) d2run in
   List.iter (fun (date,tl) ->
      let tl = List.fast_sort compare tl in
      P.printf "%4d: %s\n" date (U.string_of_list i2s tl)
   ) d2run;
   ()
)

(* UTILS:
   compute approx. start and end dates for each tasks
   e.g.
   let (begin_dates, end_dates) = compute_dates d Dag.get_preds

   N.B. since the 'get_preds' is a parameter, the same function
   can be used to compute the same info ON THE REVERSE graph
   e.g.
   let (time2end, level2end) = compute_dates d Dag.get_succs
   give:
   - time from the end of each task to the end of the whole dag
   - time from the beginning of the task to the end of the whole dag
   - N.B. the later is used as a priority in the LS algo
*)

let compute_dates d get_preds = (
   let nbt = Dag.nb_tasks d in
   (* V.printf ~f:dbg "# Enters Map.compute_dates, nbt=%d\n" nbt; *)
   let begin_dates = Array.make nbt (-1) in
   let end_dates = Array.make nbt (-1) in
   let rec get_end_date tid = (
      match end_dates.(tid) with
      | (-1) -> (
         let preds = get_preds d tid in
         let beg = List.fold_left (
            fun a pid -> max a (get_end_date pid)
         ) 0 preds in
         begin_dates.(tid) <- beg;
         end_dates.(tid) <- beg + (get_wcet d tid);
         end_dates.(tid)
      )
      | x -> x
   ) in
   for tid = 0 to (nbt-1) do
      let _ = get_end_date tid in
      ()
   done;
   (begin_dates, end_dates)
)
(* widely used in all algo *)
let compute_begin_end d = (
   let (begin_dates, end_dates) = compute_dates d Dag.get_preds in
   (* virtual_sched begin_dates end_dates; *)
   V.exe ~f:dbg (fun () ->
      let nbt = Dag.nb_tasks d in
      for tid = 0 to (nbt-1) do
         V.printf ~f:dbg "# task %d (%s): begin=%d end=%d\n"
            tid (Dag.get_task_name d tid)
            begin_dates.(tid) end_dates.(tid)
      done
   );
   (begin_dates, end_dates)
)
(* used e.g. for LS algo, only the distance COMPRISING the task wcet
   is normally used, thus throw awat the other tab
*)

let compute_dist_to_end d =
   let (_, d2e) = compute_dates d Dag.get_succs in
   V.exe ~f:dbg (fun () ->
      let nbt = Dag.nb_tasks d in
      for tid = 0 to (nbt-1) do
         V.printf ~f:dbg "# task %d (%s): distance to end=%d\n"
            tid (Dag.get_task_name d tid) d2e.(tid)
      done
   );
   d2e
