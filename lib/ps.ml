
(* module R = Ez.Rand *)
module R = Ez.Rand
module P = Printf
module V = Ez.Verbose
let dbg = V.get_flag "ParSeq"

(* 22/06/23 New version, polymorphic *)

type 'a t =
| Par of 'a t list
| Seq of 'a t list
| Node of 'a 

(* dump *)
let dump os n2s x =
   let rec rec_dump = function
      | Seq l ->
         P.fprintf os "(";
         List.iteri (fun i y -> (if i > 0 then P.fprintf os " ; "); rec_dump y) l;
         P.fprintf os ")";
      | Par l ->
         P.fprintf os "(";
         List.iteri (fun i y -> (if i > 0 then P.fprintf os " | "); rec_dump y) l;
         P.fprintf os ")";
      | Node k -> output_string os (n2s k)
   in
   rec_dump x

(* random BINARY ast *)
let random_binary pproba newnode sz = 
   let rec _recrand  sz = (
      if (sz = 1) then Node (newnode ()) 
      else (
         (* let cut = 1 + (Random.int (sz-1)) in *)
         let cut = R.int_between 1 (sz-1) in
         let ps1 = _recrand cut in
         let ps2 = _recrand (sz-cut) in
         if R.percent pproba then
            Par [ ps1; ps2 ]
         else
            Seq [ ps1; ps2 ]
      )
   )
   in _recrand sz 
   
(* Basic normalization *)
let rec _fpar (acc: 'a t list) (x: 'a t) = match x with
   | Par l -> acc @ (_norm_par l)
   | Seq l -> acc @ [Seq (_norm_seq l)]
   | Node _ -> acc @ [x]
and _fseq (acc: 'a t list) (x: 'a t) = match x with
   | Seq l -> acc @ (_norm_seq l)
   | Par l -> acc @ [Par (_norm_par l)]
   | Node _ -> acc @ [x]
and _norm_par (l: 'a t list) : 'a t list = List.fold_left _fpar [] l 
and _norm_seq (l: 'a t list): 'a t list = List.fold_left _fseq [] l 
and normalize (x: 'a t): 'a t = (
   match x with
      | Seq l -> Seq (_norm_seq l)
      | Par l -> Par (_norm_par l)
      | Node _ -> x
)

let random_normal pproba newnode sz = 
   normalize (random_binary pproba newnode sz)

(* adding source/sink, fj nodes etc
   is a concern of ps -> dag, no longer here 

(* Basic normalized random ast *)
let rand_norm ?(pproba=50) newnode wcet sz =
   let source = Node (newnode ()) in
   match sz with
   | 1 -> source
   | 2 -> (
      let sink = Node (newnode ()) in
      Seq [source; sink ]
   )
   | _ -> (
      let core = _rand pproba wcet (sz-2) in 
      let sink = Node (newnode ()) in
      let res = Seq [ source; core; sink ] in
      normalize res 
   )

(* Fork-join normalized random ast *)
let rand_fj ?(pproba=50) newnode wcet sz =
   let new_fj_task () = Node (newnode ()) in
   let rec do_seq_args fork_needed = function
   | (Par sl)::pl -> (
      if fork_needed then
         let ft = new_fj_task() in
         let p' = Par (do_par_args sl) in
         ft::p'::(do_seq_args true pl)
      else 
         let p' = Par (do_par_args sl) in
         p'::(do_seq_args true pl)
   ) 
   | ((Node _) as tt)::pl ->
      tt::(do_seq_args false pl)
   | [] when fork_needed -> [new_fj_task ()]
   | [] -> []
   | _ -> assert false
   and do_par_args (sl: t list): t list = List.map (
      function
         | (Seq pl) -> Seq (do_seq_args true pl)
         | (Node _) as tt -> tt
         | _ -> assert false
      ) sl
   in
   (* gen a normalized core *)
   let core = _rand pproba wcet sz in 
   V.exe ~f:dbg ( fun () ->
      V.xprintf ~f:dbg "(rand_fj) c0 = ";
      dump (V.os ()) core ;
      V.printf ~f:dbg "\n"
   );
   let core = normalize core in
   V.exe ~f:dbg ( fun () ->
      V.xprintf ~f:dbg "(rand_fj) c1 = ";
      dump (V.os ()) core ;
      V.printf ~f:dbg "\n"
   );
   match core with 
   | Node _ -> core
   | Seq pl -> Seq (do_seq_args true pl)
   | Par sl -> (
      let top = new_fj_task () in
      let bot = new_fj_task () in
      Seq (top::(do_par_args sl)@[bot]) 
   )

*)
