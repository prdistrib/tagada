(*
*)

type t
(* tasks (nodes)  are identified by an int *)
type tid = int
type tid_set = Ez.Utils.IntSet.t 

(* att management *)
type attval = 
[  `Int of int
|  `String of string
]
type att = string * attval
val set_att: t -> string -> attval -> unit
val get_att: t -> string -> attval
val att_seq: t -> (string * attval) Seq.t

(* edge 'width' :
- just a int (amount of data transmited) for now
- likely to evolve: keep it abstract
*)

(* hard copy *)
val copy : t -> t

(* retrieve info *)
val get_task: t -> tid -> Task.t
val get_task_name: t -> tid -> string
val get_succs: t -> tid -> tid list
val get_preds: t -> tid -> tid list

val nb_tasks: t -> int
val taskid: t -> string -> int

(* get_edge_width src dest = amount of data
   transmited from src to dest
*)
val get_ewidth: t -> tid * tid -> int

(* by construction, an edga has a single numerical
   attribute. In MIA it is interpretted as the 'width'
   (amount of data transimited) thus the name...
*)
val gen_edge_width : (Task.t -> Task.t -> int) ref

(* iterative construction, Warning: dest is the first tid
   add_trans_to d dest ?amount=42 src
   n.b. creatin size can be approximative
*)
val create: int -> att list -> t

(* add_edge_to dag d w s : add a transition 's-w->d' in dag *)
val add_edge_to: t -> tid -> int -> tid -> unit

(* add_edge_from dag s w d : add a transition 's-w->d' in dag *)
val add_edge_from: t -> tid -> int -> tid -> unit

(* version for random dag generation:
   - source and dest are Task.t not task id (int)
   - width is randomly chosen if not given
*)
val create_edge_to: t -> Task.t -> ?width:int -> Task.t -> unit

val add_task:  t -> Task.t -> unit
val remove_edge: t -> tid * tid -> unit

(* Raw dump, see ToDot, ToMia for formated ones *)
val dump: out_channel -> t -> unit


(* ParSeq to Dag
- very specific, actually more a tagada feature
- should not probably be here ...
*)
(* 1st version, translate as it, no pruning no optim *)
val of_parseq_raw: ParSeq.t -> att list -> t
(* 2nd version, with pruning policy *)
val of_parseq: ?prune:int -> ParSeq.t -> att list -> t
