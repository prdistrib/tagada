
(* helper to manage MIA attributes
TODO
does not actually "include" the Task module, just a layer
*)
module V=Ez.Verbose
module R=Ez.Rand

let (wcet, set_wcet) = Task.register_att "procDemand" 0
let (memsize,set_memsize) = Task.register_att "memSize" 0
let (memdemand, set_memdemand) = Task.register_att "memDemand" 0
let (core, set_core) = Task.register_att "assignedTo" 0
let (order, set_order) = Task.register_att "order" 0
let (release, set_release) = Task.register_att "release" 0

type t = Task.t
let to_string = Task.to_string
let dbg_string = Task.dbg_string
let id = Task.id
let card = Task.card

(* There are 2 abstract 'profiles' for
   random generation (see ParSeq & Dag rand):
   - regular tasks
   - 'system' tasks added by the generator to fullfil 
     structural requirements (e.g. fork/join)
let create ?(nme="") wcet msz = (
   let res = Task.create ~nme () in
   set_memsize res msz;
   set_wcet res wcet;
   res
)
*)

