
module P = Printf
module H = Hashtbl
(* UTILS *)
module U = Ez.Utils
module V = Ez.Verbose
(* open Tagada *)
open Map

module T = MiaTask

let dbg = V.get_flag "algoLS"

(* Sort of 'classic' list-schedulling
*)

(*
TODO there params should be handled in a cleaner way
   - directly taken from the MPPA3 study
   - max (local) memory for a core 256
   - max cores = 5 clusters * 16 cores

   TODO fix algo to take into account priority when picking from the queue:
      - priority is given to the higher 'static-level'
      - static level = +/- its distance to the end of the whole program (?)
      - sl(x) = wcet(x) + MAX(sl(y) / x->y)
*)

exception Not_enough_memory 

let doit
   ?(max_mem:int=max_int) ?(nb_cores:int=8)
   (input_dag:Dag.t)
= (
   V.printf ~l:2 "# AlgoLS.doit max_mem:%d nb_cores:%d\n" max_mem nb_cores;
   (* hard copy input dag since it will evolved during mapping *) 
   let zedag = Dag.copy input_dag in 
   let nbt = Dag.nb_tasks zedag in
   (* "earliest" begin and end date of each task *)
   let (bda,_eda) = compute_begin_end zedag in
   (* "global end distance" of each task *)
   let d2e = compute_dist_to_end zedag in
   (* deps counter *)
   let deps = Array. init nbt (fun i -> List.length (Dag.get_preds zedag i)) in
   (* init task-to-map queue with those with deps = 0
      priority is given to the max d2e
   *)
   let cmp i j = -(compare d2e.(i) d2e.(j)) in
   let zequeue = Pqueue.create ~cmp:cmp () in
   let _ = Array.iteri (fun t d -> if (d = 0) then Pqueue.push zequeue t) deps in
   (* Init the map structure
      n.b. This algo
      - maps on exactly nb_cores
      - set max_cores to nb_cores
      - force to use all cores
      - only fails if not enough memory
   *)
   let zemap = Map.create nb_cores ~useallcores:true  max_mem in

   while not (Pqueue.is_empty zequeue) do
      let tid = Pqueue.pop zequeue in
      let tbda = bda.(tid) in
      let tsk = Dag.get_task zedag tid in
      (* itakes core mem size into account *)
      let has_enough_mem core =
         V.printf ~f:dbg 
            "#  has_enough_mem: mem required by task(%d)=%d,  left mem in core(%d)=%d\n"
            tid (T.memsize tsk) core.Vcore._index core.Vcore._free_mem;
         (T.memsize tsk) <= core.Vcore._free_mem
      in
      (* try to schedule on allocated core without delay *)
      let cmp_delays core1 core2 = (
         (* returns the core impliying smallest delay *)
         let d1 = tbda - core1.Vcore._end_date in
         let d2 = tbda - core2.Vcore._end_date in
         match (d1 < 0, d2 < 0) with
         | (true, true) 
         | (false, false) ->
         (* both positive or negative, takes the closest to 0 *)
            Stdlib.compare (abs d1) (abs d2)
         (* one positive other negative, takes the positive *) 
         | (true, false) -> 1
         | (false, true) -> (-1)
      ) in
      let cix = match Map.find_least_core
            zemap
            ~suitable:has_enough_mem
            cmp_delays
         with
         | Some c -> c
         (* | None ->  Map.new_core zemap *)
         | None -> raise Not_enough_memory 
      in
      Map.add_task zemap cix ~startd:tbda  tsk;
      let update_deps i = ( 
         deps.(i) <- deps.(i)-1;
         if deps.(i) = 0 then Pqueue.push zequeue i
      ) in
      List.iter update_deps (Dag.get_succs zedag tid)
   done;
   (* V.exe ~f:dbg (fun () -> *)
   V.exe ~l:2 (fun () ->
      V.printf ~f:dbg "# Mapping:\n";
      Map.dump stderr zemap
   );
   (* change attributes in zedag *) 
   Dag.set_att zedag "numOfCores" (`Int (Map.nb_cores zemap));
   zemap |> Map.iter_cores (fun cix tseq ->
      tseq |> Seq.iteri (fun o tsk ->
         T.set_core tsk cix;
         T.set_order tsk o;
      )
   );
   zedag
)
