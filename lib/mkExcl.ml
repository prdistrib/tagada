(*
Experimental:

Given a ParSeq.t and a proba x%
generates a list of 'excluded'

*)

open ParSeq
module T = Task
module R = Ez.Rand
module U = Ez.Utils

let dbg = V.get_flag "MkExcl"

let product l l' = 
   let l = match l with [] -> [[]] | _ -> l in
   let l' = match l' with [] -> [[]] | _ -> l' in
  List.concat (List.map (fun e -> List.map (fun e' -> (e@e')) l') l)

(* randomly split into 2 non empty list *)
let rand_split l =
   match List.partition (fun _ -> R.bool ()) l with
   | ([], x::y::r)
   | (x::y::r, []) -> ([x], y::r)
   | other -> other 

(*
rec gen returns:
- list of tasks in the branch
- list of xed WHITHIN this branch
*)

let tl2s tl = U.string_of_list T.to_string tl
 
let f (proba:int) (ps: ParSeq.t) : T.t list list = (
   V.xprintf ~f:dbg "f %d\n" proba;
   let upseq = function
      | [] -> assert false
      | [x] -> x
      | l -> Seq l
   in
   let uppar = function
      | [] -> assert false
      | [x] -> x
      | l -> Par l
   in
   let rec gen ps : T.t list * T.t list list = (
      match ps with
      | Seq [a;b] -> (
         let (ta, exda) = gen a in
         let (tb, exdb) = gen b in
         (ta @ tb, product exda exdb)
      )
      | Seq [] | Seq [_] -> assert false
      | Seq l -> let (a,b) = rand_split l in
         gen (Seq [upseq a; upseq b])
      | Par [a;b] -> (
         let (ta, exda) = gen a in
         let (tb, exdb) = gen b in
         if R.percent proba then (
            (* either a or b are executed:
               if a then tb UNION exda are excluded
               if b then ta UNION exda are excluded
            *)
V.exe  ~f:dbg (fun () ->
   V.xprintf ~f:dbg "gen excl\n";
   V.xprintf ~f:dbg "ta=%s\n" (tl2s ta);
   V.xprintf ~f:dbg "tb=%s\n" (tl2s tb);
);
            (ta @ tb, (product exda [tb]) @ (product exdb [ta]))
         ) else (
            (ta @ tb, exda @ exdb)
         )
      )
      | Par [] | Par [_] -> assert false
      | Par l -> let (a,b) = rand_split l in gen (Par [uppar a; uppar b])
      | Task t -> ([t], [])
   ) in
   let (_, exds) = gen ps in
   exds
)
