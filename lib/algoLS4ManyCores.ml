
module P = Printf
module H = Hashtbl
(* UTILS *)
module U = Ez.Utils
module V = Ez.Verbose
(* open Tagada *)
module T = MiaTask
open Map

let dbg = V.get_flag "algoLS"

(* TODO there params should be handled in a cleaner way
   - directly taken from the MPPA3 study
   - max (local) memory for a core 256
   - max cores = 5 clusters * 16 cores

   TODO fix algo to take into account priority when picking from the queue:
      - priority is given to the higher 'static-level'
      - static level = +/- its distance to the end of the whole program (?)
      - sl(x) = wcet(x) + MAX(sl(y) / x->y)
*)

      
(* Adaptation of classic LS for Kalray manycore archi :
   Since the number of (potential) cores is huge,
   we first try to schedule tasks WITHOUT delaying their 'virtual' early start date,
   i.e. if it is not possible to schedule a task as soon as possible on already used cores,
   we allocate a new empty one for it, ass long as the maxcores is not reached.
   When maxcores is reached and we are obliged delay them:
   - we apply the classical LS heuristic (take the core with the lower delay)
*)

let doit
   ?(max_mem:int=max_int) ?(max_cores:int=8)
   (input_dag:Dag.t)
= (
   V.printf ~f:dbg "# Enters Map.doit\n";
   (* hard copy input dag since it will evolved during mapping *) 
   let zedag = Dag.copy input_dag in 
   let nbt = Dag.nb_tasks zedag in
   (* "earliest" begin and end date of each task *)
   let (bda,_eda) = compute_begin_end zedag in
   (* "global end distance" of each task *)
   let d2e = compute_dist_to_end zedag in
   (* deps counter *)
   let deps = Array. init nbt (fun i -> List.length (Dag.get_preds zedag i)) in
   (* init task-to-map queue with those with deps = 0
      priority is given to the max d2e
   *)
   let cmp i j = -(compare d2e.(i) d2e.(j)) in
   let zequeue = Pqueue.create ~cmp:cmp () in
   let _ = Array.iteri (fun t d -> if (d = 0) then Pqueue.push zequeue t) deps in
   (* init the map structure *)
   let zemap = Map.create max_cores max_mem in

   while not (Pqueue.is_empty zequeue) do
      let tid = Pqueue.pop zequeue in
      let tbda = bda.(tid) in
      let tsk = Dag.get_task zedag tid in
      (* try to schedule on allocated core without delay *)
      let is_optimal_core core = (
         (* enough mem ? *)
         if core.Vcore._free_mem < (T.memsize tsk) then false
         else tbda >= core.Vcore._end_date
      ) in
      let cix = match Map.find_first_core zemap is_optimal_core with
         | Some c -> c
         | None ->  Map.new_core zemap
      in
      Map.add_task zemap cix ~startd:tbda  tsk;
      let update_deps i = ( 
         deps.(i) <- deps.(i)-1;
         if deps.(i) = 0 then Pqueue.push zequeue i
      ) in
      List.iter update_deps (Dag.get_succs zedag tid)
   done;
   V.exe ~f:dbg (fun () ->
      V.printf ~f:dbg "# Mapping:\n";
      Map.dump stderr zemap
   );
   zemap
)
