
(* global stuff *)

let exit msg =
   Printf.fprintf stderr "Error: %s\n" msg;
   exit 1
