
(* module R = Ez.Rand *)
module R = Ez.Rand
module U = Ez.Utils

let _taskcpt = ref 0

(*
Task can be attached up to 10 attributes that
MUST BE INTEGERS
- Attibutes MUST be registered BEFORE
  the creation of any task
*)


type t = {
   id: int;
   nme: string;
(*
   wcet: int;
   memsize: int;
*)
   atts: int array;
}

(* global management of attributes
   max. 10 attributes
*)
let _MAX_ATTS = 10
let nb_atts = ref 0
let att_tab = Array.make 10 ("UNUSED", 0)

let register_att name default = (
   let atix = !nb_atts in
   incr nb_atts;
   att_tab.(atix) <- (name, default);
   let geta tsk = tsk.atts.(atix) in
   let seta tsk v = tsk.atts.(atix) <- v in
   (geta, seta)
)

let att_list t = (
   let rec mka i =
      if i >= _MAX_ATTS then []
      else (fst (att_tab.(i)), t.atts.(i))::(mka (i+1))
   in
   mka 0
)

(* register an attribute for all task, returns*)

let id t = t.id
(*
let memsize t = t.memsize
let wcet t = t.wcet
*)

(* create a task and auto-name it *)
let create ?(nme="") () = (
   let id = !_taskcpt in
   let nme = match nme with
      | "" -> Printf.sprintf "t%02d" id
      | _ -> nme
   in
   let atts = Array.make _MAX_ATTS 0 in
   incr _taskcpt;
   {id; nme; atts}
)

let card () = !_taskcpt
(* compatible with Ez.Utils XTypes *)
let to_string x = x.nme
let compare = Stdlib.compare

let at2s (k,v) = Printf.sprintf "%s=%d" k v

let dbg_string x =
   let atl = att_list x in
   x.nme^" "^(U.string_of_list at2s atl)

(* OBSOLETE
Extra-Functional Info management
(* Memory footprint (size) based on pseudo-average *)
let _MAXMEM = ref  256
let set_max_memsize x = _MAXMEM := x
(* low profile ~10% of _MAXMEM *)
let low_ms() = min !_MAXMEM (R.int_around((10 * !_MAXMEM)/100))
(* normal profile ~30% of _MAXMEM *)
let normal_ms() = min !_MAXMEM (R.int_around((50 * !_MAXMEM)/100))
(* high profile ~70% of _MAXMEM *)
(* let high_ms() = min !_MAXMEM (R.int_around((70 * !_MAXMEM)/100)) *)
*)
