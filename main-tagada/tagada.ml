module P = Printf
module H = Hashtbl

let _TOOLPATH = Sys.argv.(0)
let _TOOLNAME = Filename.basename _TOOLPATH

let _TOOLVERSION = "0.5" (* add -mial *)
(* add utils *)
(* let _TOOLVERSION = "0.4" *)
(* big cleanup -> distrib *)
(* let _TOOLVERSION = "0.3" *)
(* add -map arg *)
(* let _TOOLVERSION = "0.2.b" *)
(* add -excl arg *)
(* let _TOOLVERSION = "0.2.a" *)
(* let _TOOLVERSION = "0.1" *)
let usage_msg = P.sprintf "%s version %s\nusage: %s <options> name | %s -help"
   _TOOLNAME _TOOLVERSION _TOOLNAME _TOOLNAME 

(* let _ = P.printf "%s\n" _TOOLPATH *)

module U = Ez.Utils
module V = Ez.Verbose
module R = Ez.Rand
open Tagada
module T = MiaTask
module MU = MiaUtils
module VS = ValueSpec

(* dump dag as MIA-compliant yaml *)


(* ARGS + MAIN 
  Dotx = detailed label for node 
*)
type out_format = Mia | Mial | Dot | Dotx
let enum_arg (l: (string * 'a) list) (r: 'a ref) : Arg.spec = (
   let set_enum_arg (s : string): unit =
      r := List.assoc s l 
   in
   Arg.Symbol (List.map fst l, set_enum_arg)
)
let ___enum_arg (l: (string * 'a) list) (r: 'a ref) : (string list * (string -> unit)) = (
   let set_enum_arg (s : string): unit =
      r := List.assoc s l 
   in
   (List.map fst l, set_enum_arg)
)

(* OUT_FORMAT: Mia (yaml) or Dot *)
let arg_list = ref []
 let rec usage () = (
   P.printf "%s" usage_msg;
   Arg.usage (!arg_list) "";
   exit 0
)
and _OUT_FORMAT = ref Mia 
and out_format_arg () = !_OUT_FORMAT
(* SYSNAME: the ident of the system *)
and _SYSNAME = ref ""
and set_default (x:string) : unit =
   try (
      assert (!_SYSNAME = "");
      _SYSNAME := x;
   ) with _ -> (
      Arg.usage (!arg_list) ("unexpected argument \""^x^"\"\n"^usage_msg) ;
      exit 1
)
and sys_arg () : string =
   if (!_SYSNAME = "") then (
      Arg.usage (!arg_list) ("system name is mandatory\n"^usage_msg) ;
      exit 0
   );
   !_SYSNAME
(* SIZE: number of tasks *)
and _SIZE = ref 10
and size_arg () : int =
   try (
      assert (!_SIZE > 0);
      !_SIZE
   ) with _ -> (
      Arg.usage (!arg_list) ("unexpected size argument \""^(string_of_int !_SIZE)^"\"\n"^usage_msg) ;
      exit 1
   )
(* BRANCH REDUCTION *)
and _PRUNE = ref 0
and prune_arg () =
   try (
      assert ((!_PRUNE >= 0) && (!_PRUNE <= 100));
      !_PRUNE
   ) with _ -> (
      Arg.usage (!arg_list) ("unexpected prune argument \""^(string_of_int !_PRUNE)^"\"\n"^usage_msg) ;
      exit 1
   )
(* SEED *)
and _SEED = ref None
and seed_arg () = !_SEED
and set_seed s = _SEED := Some s
(* WCET: approx. ideal duration of the critcal path *) 
and _WCET = ref 0
and wcet_arg () : int =
   if (!_WCET = 0) then _WCET := 100 * (size_arg ());
   !_WCET
(* MSIZE: max memory footprint of one task *)
and _MSIZE = ref 0
and _FORKJOIN = ref false
and msize_arg () = !_MSIZE
(* SPEC ATTRIBUTES *)
and _MDEMAND_SPEC = ref None
and _MSIZE_SPEC = ref None
and _EWIDTH_SPEC = ref None
and set_spec _V s = (_V := Some (VS.of_string s))
and set_spec_pc _V s = (_V := Some (VS.of_string ~percent:true s))

(* PARALLEL RATIO IN PERCENT: 0 -> full sequential, 100 -> full para *)
and _PRATIO = ref 50
and pratio_arg () =
   try (
      assert ((!_PRATIO >= 0) && (!_PRATIO <= 100));
      !_PRATIO
   ) with _ -> (
      Arg.usage (!arg_list) ("unexpected parallel ratio argument \""^(string_of_int !_PRATIO)^"\"\n"^usage_msg) ;
      exit 1
   )
and _OUTFOLDER = ref "."
and _DUMP = ref false
and _EXCL = ref 0
and _DODOT = ref false
and _DODOTX = ref false
and _DOMAP = ref 0

(* SEED *)
(*==== ARGS LIST ====*)
let _ = arg_list :=
let dbgflags = V.flag_list () in
[
   ("\rReproductibility:", Arg.Unit (fun () -> ()), "  ");

   ("-seed", Arg.Int set_seed, "<int>  init random with seed (default self_init)");
   (* ("-t",  Arg.Symbol ([ "mia"; "dot" ], fun s -> _OUT_FORMAT := s), ": set output type (default mia)"); *)

   ("\rGraph structure:", Arg.Unit (fun () -> ()), "  ");
   ("-s",  Arg.Set_int  _SIZE,            "<int>   number of tasks (default 10)");
   ("-fj", Arg.Set _FORKJOIN, "  fork/join style (-prune ignored)");
   ("-prune", Arg.Set_int _PRUNE, "<percent> edge pruning proba, 0 none, 100 full (default 0)");
   ("-pratio", Arg.Set_int _PRATIO, "<percent>  parallel ratio, 0 fully sequential, 100 fully parallel (default 50)");

   ("\rOutput:", Arg.Unit (fun () -> ()), "  ");
   ("-t",  (enum_arg [("mia", Mia); ("mial", Mial); ("dot", Dot); ("dotx", Dotx)] _OUT_FORMAT), "   set output type (default mia)");
   (* ("-ls", Arg.Set_int _DOLS, "<nbcores>: (MIA) computes a core mapping based on list-schedulling algo"); *)
   ("-dot", Arg.Set _DODOT ,  "  force dot output  (whatever is the output type, use only one of -dot -dotx)"); 
   ("-dotx", Arg.Set _DODOTX ,  "   force dotx output (whatever is the output type, use only one of -dot -dotx)"); 
   ("-map", Arg.Set_int _DOMAP, "<nb-cores>  (MIA) computes a basic mapping of the tasks on nb-cores");

   ("\rGraph attributes:", Arg.Unit (fun () -> ()), "  ");
   ("-w",  Arg.Set_int  _WCET,            "<int>\tapproximative global WCET (default 100*nb tasks)");
   ("-md", Arg.String (set_spec_pc _MDEMAND_SPEC), "<ratio spec>\tmemory demand, as approximate ratio of wcet, e.g. ~10, [10,20] (default ~20)");

   ("-m",  Arg.Set_int  _MSIZE,         "<int>  (ignored if -ms ) maximal memory for 1 task (default 256)");
   ("-ms", Arg.String (set_spec _MSIZE_SPEC), "<int spec>\tmin and max memory size for each task");
   ("-ew", Arg.String (set_spec _EWIDTH_SPEC), "<int spec>\tamount of data transmited on each edge, ~10, [100,200] (default ~20)");

   (* ("-md", Arg.Tuple ([Arg.Int (set_low_range md_range) ; Arg.Int (set_high_range md_range)]), "<min <max>: min and max memory demand for each task"); *)
   (* ("-ew", Arg.Tuple ([Arg.Int (set_low_range ew_range) ; Arg.Int (set_high_range ew_range)]), "<min <max>: min and max edge width"); *)

   ("\rExtra/experiment:", Arg.Unit (fun () -> ()), "  ");
   ("-excl", Arg.Set_int _EXCL, "<percent>   (EXPERIMANTAL) gen exclusion lists, arg is the exclusive proba (default 0)");

   ("\rMisc:", Arg.Unit (fun () -> ()), "  ");
   ("-v",  Arg.Unit (fun () -> V.set 1),  "  set verbose mode");
   ("-vv", Arg.Unit (fun () -> V.set 2),  "  more verbose mode");
   ("-dbg", Arg.Symbol (dbgflags,
      (function s -> let x = V.get_flag s in V.set_flag x)), " set debug flag");
   ("-of",  Arg.Set_string  _OUTFOLDER,      "<folder>   set out folder (default .)");
   ("-dump",  Arg.Set  _DUMP,      " : dump the output file on stdin after generation");
   ("--version", Arg.Unit (fun () -> print_string _TOOLVERSION; exit 0),  "  print version and exit");
]

(* util *)
let cat f =
   let ic = open_in f in
   try (
      while true do
         print_string (input_line ic);
         print_string "\n"
      done
   ) with _ -> (
      close_in ic
   )

(*
The module in charge of actually creating tasks is ParSeq
*)

(* TODO obsolete
OBSO- commandline args to algo parameters ...
OBSO- ParSeq requires gen_mem_size & gen_low_mem_size
OBSO- ToMia requires gen_mem_demand

The creation of Mia-compliant task is NOW centralized in MiaTask,
transitory solution: MiaTask inherits the management of
gen_mem_size, gen_low_mem_size, gen_mem_demand
TODO reorganize a little to cleanup all that ...

*)

(* creates the two functions for generating
random MIA-compliant tasks (to be passed to ParSeq.rand)
Returns 2 creators for normal and extra (e.g. fork-join) tasks
*)
type i2t = int -> T.t
type u2t = unit -> T.t

let make_task_creators () : i2t * u2t = (
(* Memory size:
- basic management (historical but not recommanded) 
   * user specify a max memsize (default 256)
   * gen_mem_size randomly chose a size around 50% of max
   * gen_low_mem_size randomly chose a size around 10% of max
- total management: 
   * user specify a random spec (either "~v" or "[min,max]"
   * gen_mem_size randomly chose a size according to the spec
   * gen_low_mem_size chose a size according to a lowered spec
*)
   let (normal_ms, low_ms) = match !_MSIZE_SPEC with
   | None -> 
      (* basic ... *)
      let max_ms = match !_MSIZE with v when (v > 0) -> v | _ -> 256 in
      let low_ms() = min max_ms (R.int_around((10 * max_ms)/100)) in
      let normal_ms() = min max_ms (R.int_around((50 * max_ms)/100)) in
      (normal_ms, low_ms)
   | Some (Around n) ->
      let low_ms() = R.int_around (1 + n/10) in
      let normal_ms() = R.int_around n in
      (normal_ms, low_ms)
   | Some (Range (l,h)) ->
      let low_ms() = R.int_around (1 + l/10) in
      let normal_ms() = R.int_between l h in
      (normal_ms, low_ms)
   in
(* Memory demand:
- gen_mem_demand: (int -> int -> int) ref
   * 1st arg is the actual memsize of the task
   * 2nd arg is the actual wcet of the task
- default is to take some ratio of wcet (ignore mem size) 
   * can be specified by _MDEMAND_SPEC
   * default is ~20%
*)
   let mdspec = match !_MDEMAND_SPEC with Some s -> s | None -> VS.Around 20 in
   let pc r v = (r*v)/100 in
   let gen_md  = match mdspec with
   | VS.Around a -> fun _ wc -> R.int_around (pc a wc)
   | VS.Range (l,h) -> fun _ wc -> R.int_between (pc l wc) (pc h wc)
   in
   let create_regular wcet =
      let res = Task.create () in
      let ms = normal_ms () in
      let md = gen_md ms wcet in
      T.set_wcet res wcet;
      T.set_memsize res ms;
      T.set_memdemand res md;
      res
   in
   let create_extra () =
      let res = Task.create () in
      let wcet = R.int_around 100 in (* TODO NOT SATISFACTORY *)
      let ms = low_ms () in
      let md = gen_md ms wcet in
      T.set_wcet res wcet;
      T.set_memsize res ms;
      T.set_memdemand res md;
      res
   in
   (create_regular, create_extra)
)

let make_gen_edge_width () : T.t -> T.t -> int = (
(* Edge width:
   Dag is in charge of assigning a width to each edge through the function:
   - Dag.gen_edge_width src tgt: int
   - TODO ultra simple solution here none of src and tgt are taken into account
*)
   let ewspec = match !_EWIDTH_SPEC with Some s -> s | None -> VS.Range (1,10) in
   let gew = match ewspec with
   | VS.Around a -> fun _ _ -> R.int_around a 
   | VS.Range (l,h) -> fun _ _ -> R.int_between l h
   in
   gew
)

let main () = (
   arg_list := Arg.align !arg_list;
   Arg.parse (!arg_list) set_default usage_msg;

   (* management of random attributes *)
   let (normal_task, extra_task) = make_task_creators () in
   ParSeq.gen_normal_task := normal_task;
   ParSeq.gen_extra_task := extra_task;
   let gew = make_gen_edge_width () in
   Dag.gen_edge_width := gew; 

   let sysname = sys_arg () in
   (* OBSOLETE Task.set_max_memsize (msize_arg()); *)
   let seed = match seed_arg () with
      | None -> (R.self_init (); R.int_between 0 1000000)
      | Some s -> s
   in
   R.init seed;
   let size = size_arg() in
   V.printf "# %s: size:%d seed:%d system:%s\n" _TOOLNAME  size seed sysname;
   let pratio = pratio_arg () in
   V.printf "# building parseq exp ...\n";
   let ps = if !_FORKJOIN then
      ParSeq.rand_fj ~pproba:pratio (wcet_arg()) size
   else
      ParSeq.rand_norm ~pproba:pratio (wcet_arg()) size
   in
   (* EXPERIMENTAL gen list of excluded *)
   if !_EXCL <> 0 then (
      let xfile = sysname^".excl" in
      let xoc = open_out xfile in
      let xlist = MkExcl.f !_EXCL ps in
      xlist |> List.iter (fun x ->
         output_string xoc 
            ((U.string_of_list ~obr:"" ~cbr:"" Task.to_string x)^"\n")
      );
      close_out xoc
   );
   let prune = if !_FORKJOIN then 0 else prune_arg() in
   V.printf "# building dag ...\n";
   let dag = Dag.of_parseq ~prune:prune ps MU.dag_atts in

   (* basic mapping ? *)
   let nb_cores = !_DOMAP in
   let dag =
       if (nb_cores > 0) then (
         V.printf "# computing mapping on %d cores...\n" nb_cores ;
         AlgoLS.doit ~nb_cores:nb_cores dag
      ) else dag
   in

   (* ADD sysName attribute *)
   Dag.set_att dag "sysName" (`String !_SYSNAME);

   let do_mia () = ToMia.dump_yaml ~folder:(!_OUTFOLDER) sysname dag in
   let do_mia_light () = ToMia.dump_yaml ~noprofiles:true ~folder:(!_OUTFOLDER) sysname dag in
   let do_dot () = ToDot.dump ~folder:(!_OUTFOLDER) sysname dag in
   let do_dotx () = ToDot.set_node_label "nwm"; do_dot () in
   (* main output *)
   let outf = match out_format_arg () with
      | Mia -> do_mia ()
      | Mial -> do_mia_light ()
      | Dot -> do_dot ()
      | Dotx -> do_dotx ()
   in
   (* additional output *)
   if !_DODOT then do_dot () |> ignore;
   if !_DODOTX then do_dotx () |> ignore;
   if !_DUMP then cat outf
)

let _ = main ()
