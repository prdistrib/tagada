﻿# Tagada versions changelog


## V0.3
- Apr. 2023
- 2nd distributed version
- commandline arguments reorganized
- new spec style for random numbers, e.g.:
   - "~20" : around 20
   - "[50,80] : berween 50 and 80
- new functionality "-map <nbcores>"
   - computes (a simple) mapping of the task of nb-cores
   - uses a simple list-schedulling algo.

## V0.2
- Jan. 2023
- experimental

## V0.1
- Aug. 2022
- first distributed version
